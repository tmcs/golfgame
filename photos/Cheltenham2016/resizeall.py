#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function

import subprocess
import os
import glob


def getwidth(fn):
  widthoutput = subprocess.check_call(["identify", "-format", '"%w"', fn])
  width = int(widthoutput)
  
def makeallsmaller(desirewidth, outdir):

  for fn in glob.glob("*.jpg") + glob.glob("*.JPG"):
    if os.path.isfile(fn):
      print ("Making image", fn, "smaller")
      subprocess.check_call(["convert", fn, "-resize" ,"{}>".format(desirewidth), 
                                        os.path.join(outdir,fn)])
      
    
if __name__ == '__main__':

  outdir = "smaller"  

  if not os.path.exists(outdir):
      os.makedirs(outdir)

  makeallsmaller(400, outdir)


