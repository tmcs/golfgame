CHEM GOLF 
===========

![Chem Golf Logo](Branding/Logo.png "Chem Golf Logo")


**Chemists** are scientists who work with [**molecules**](http://whatis.techtarget.com/definition/molecule). 
Whether you want
*big molecules*, *small molecules*, *molecules to cure cancer*, or *molecules 
to clean up the atmosphere* - a chemist is your friend. 

Chemists can transform one molecule into another by using a 
[**chemical reaction**](https://en.wikipedia.org/wiki/Chemical_reaction).
They do this by *heating*, *mixing*, *boiling*, or *blasting with lasers*... 

But it's not always as easy as it sounds. This is because every reaction has a 
[**barrier**](https://en.wikipedia.org/wiki/Activation_energy).
The barrier is an invisible force field that 
[prevents the reaction from happening](https://employees.csbsju.edu/cschaller/Reactivity/kinetics/rkbarrier.htm).
Chemists need to overcome this barrier to create the molecule they want.

In ChemGolf, you play the role of a chemist trying to initiate a chemical reaction.
You (the golf ball) start at the **reactant** molecule. From here, your aim is to 
move across the 
[**Potential Energy Surface**](https://en.wikipedia.org/wiki/Potential_energy_surface),
over the barrier, and arrive at the **product** molecule.

To do this, you will need to use all the tools at your disposal:
Be that an increase in [**temperature**](https://en.wikipedia.org/wiki/Temperature),
a [**laser**](https://en.wikipedia.org/wiki/Laser), 
or a [**catalyst**](http://www.chem4kids.com/files/react_catalyst.html).

Think you know chemistry? Think you know golf? Think again! Play the 
latest beta version [here](http://drive.google.com/open?id=175qELG00FCajF3GurfDaqn8vKo7WmC6z).

Screenshots
-------------

![Screenshot of h3 level](Screenshots/golfgame_0.6/ontee_newskybox_cropped.png "Screenshot of h3 with the ball on tee")

Using pre-compiled versions
----------------------------
You can download and play the most recent version of Chem Golf here:

http://drive.google.com/open?id=175qELG00FCajF3GurfDaqn8vKo7WmC6z

Simply unzip the ".zip" file and double click on the game executable.


Compiling and developing the game yourself
-------------------------------------------
Please try compiling the game on your computer. Then put any problems you 
encounter on the [Issue Tracker](https://gitlab.com/tmcs/golfgame/issues). 

This really helps us out.

You'll need to follow the instructions here:

https://gitlab.com/tmcs/golfgame/wikis/how-to-build

Controls
------------
The controls depend what mode you are in. Use `Q` to switch between modes.

**World mode:** Use `W,A,S,D` to move the camera around. Use `E` then `W,A,S,D`
                to manually move the ball around the surface (ie to cheat!)

**Putt mode:**  Use `W,A,S,D` to move the putting arrow left and right. Then hold
                down the space bar to putt the ball with a particular impulse.

**Whole molecule mode:**  Use `W,A,S,D` to move the 
                         molecule around. (Currently broken (see Issue #63))

You can use `Z` at any time to freeze the ball. Then putt it to unfreeze it.

References
-------------

The `UnityMol` program was an inspiration for some of the code in the game.

    Z. Lv, A. Tek, F. Da Silva, C. Empereur-mot, M. Chavent and M. Baaden:
    "Game on, Science - how video game technology may help biologists tackle
    visualization challenges" (2013), PLoS ONE 8(3):e57990.
    doi:10.1371/journal.pone.0057990









