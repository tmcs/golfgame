
Chemistry is just like golf.

Say you are trying to turn this molecue in my right hand into the one in my left
hand. The molecule in my left hand might be a drug molecule, the kind that could 
cure cancer.
And the one in my right could be some starting material. 

Or suppose instead that the molecule in my right hand is a nasty poisonous molecule
from a diesel exhaust, the kind that are filling our cities pollution.
And the one in my left hand is a safe, stable, harmless molecule.

How can I turn this. Into this. Without making any of this? 
<point to jamals molecule> This is the problem
 Chemists try to solve.

You see, Golfers use their skill with a club to get a ball from one *place* to another. 
Chemists find the right conditions. Temperature, pressure, catalyst. That make
one molecule change into another.

Now Tim, I hear you say. We still don't believe that chemistry is anything like
golf. What are you on about? <jamal gets game up>

Well here's the proof. This here is the simplest chemical reaction you can think 
of. It happens in the atmosphere all the time. Here on the right we have an oxygen
molecule, and the left we have an oxygen atom (or an oxygen "radical" as we call
it in chemistry). Jamal is a chemist, and he wants to join this oxygen atom to this one.

But it's going to take some energy. So Jamal give us a bit of sunlight energy.

And there we have it. Ladies and gentlemen, you just witnessed a molecule transform
into another before your very eyes.

Hopefully we've convinced you that chemistry is a bit like golf. Thankyou



