![Golf ball on tee](../Screenshots/golfgame_0.6/ontee_newskybox_cropped.png "Chem Golf with ball on tee")


Have you ever struggled getting your head around Chemistry?
Or thought that Chemistry needs to involve mixing smelly things in a lab?

In June 2015, **ChemGolf** was born. ChemGolf is a video game that introduces
complex concepts in Chemistry (such as Reaction Kinetics) using a virtual 
reality golf course.

One year later, and ChemGolf is almost ready for release.

The aim of ChemGolf is to inspire the next generation of 
**Theoretical Chemists** and **Computational Chemists**: 
Chemists who do no experiments, but nevertheless make world-changing 
discoveries.

In ChemGolf, you play the role of a chemist trying to 
initiate a chemical reaction. You (the golf ball) start at the 
**reactant** molecule. From here, your aim is to move across the 
**[Potential Energy Surface](https://en.wikipedia.org/wiki/Potential_energy_surface)**,
over the barrier, and arrive at the **product** molecule.

To do this, you will need to use all the tools at your disposal: 
Be that an increase in **[temperature](https://en.wikipedia.org/wiki/Temperature)**,
a **[laser](https://en.wikipedia.org/wiki/Laser)**, or a 
**[catalyst](http://www.chem4kids.com/files/react_catalyst.html)**.

**Interested? Follow our progress on Twitter (https://twitter.com/chemgolfgame).
Come find us at the Cheltenham Science Festival on 10-11 June 2016.**

