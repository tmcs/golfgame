CHELTENHAM SCIENCE FESTIVAL 2016
================================

**With birdies, bogeys, drives and putts the ChemGolf outreach held it's inaugural session at the Cheltenham Science Festival. For two days, volunteers were on site to show off the game, and take questions on chemistry, computer science and the life of a PhD student.**

![Timwithstall](../photos/Cheltenham2016/smaller/IMG_20160610_134211.jpg)



**Children of all ages and adults alike visited the stall, trying to beat the scores of their friends and families. The interactive Wii remote and large screen received the best response, with school groups working together to complete the game. Despite the high level of golf over the weekend, the first hole-in-one on the game is still available!**



![Crowd](../photos/Cheltenham2016/smaller/4.JPG )



**Peter, a teenage gamer was particularly impressed with the game - "I always suspected video game technology could be used for science... I never thought about computational chemistry!".**



**The level models the simplest chemical reaction H+H2. The golf-ball hydrogen atom moves across the Potential Energy Surface, over the activation barrier to reach the product.**



![stallandposter](../photos/Cheltenham2016/smaller/IMG_20160610_134308.jpg)

**Thanks to everyone involved with the project, particularly Syma Khalid and Martin Galpin from the TMCS CDT, and Tony Curran and Steve Dorney and the Southampton Roadshow volunteers.**


**If you came to see us then we would love to hear from you! Please get in contact via [twitter](https://twitter.com/chemgolfgame). If you enjoyed the game, and would like to play again, or would like to see what you missed out, the newest version of the game is available to [download](http://www.chm.bris.ac.uk/~tw15452/golfgame/).**



**Thanks to everyone involved with the project, particularly Syma Khalid and Martin Galpin from the TMCS CDT, and Tony Curran and Steve Dorney and the Southampton Roadshow volunteers.**



If you would like to join in with this project, either with the game development or outreach opportunities then please also get in touch via [twitter](https://twitter.com/chemgolfgame)!



![group](../photos/Cheltenham2016/smaller/CkmnLlRWYAAPsyG.jpg )

