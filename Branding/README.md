`Logo.svg` and `Icon.svg` are both Inkscape SVG files. To produce the `.pdf`
and `.png` versions, run 

    inkscape Logo.svg --export-pdf=Logo.pdf --export-png=Logo.png
    inkscape Icon.svg --export-pdf=Icon.pdf --export-png=Icon.png


