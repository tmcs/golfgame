Now
#####

To ask steve dorney
======================
- powerpoint or poster or both?
- number of projectors?
- oculus rift

Bugs to fix later
===================
unloading of levels


Task 0 - Terrain
=================
Make a set of sky box walls that look like a real golf course (with forests etc).
Make some trees models, rocks models (important to make the game more interesting)
 and water to go on the surface. Make a hole model.

(Task 1- Done)
===============

Task 2 - Ball
==============
Change the gravity so it can easily get up hills etc. increase the putting impulse.
Make it behave like a real golf ball.

Task 3 - Molecule visuals (Dom)
=================================
- Make a full-screen molecular camera

Task 3.5 - moving molecule (Tim)
=================================
- Load a new square when moving the ball
- dashed lines on PES
- Does the respawn button work when the golfball script is deactivated?
- INTERNET: Does C sharp automatically copy array data?


Task 4 - Molecular geometries (Jon)
=====================================
- Modify the python script that generates the geometry data file. Currently the 
  script (`jonsrendering/savegeometries.py`) loads a binary file, modifies it, and outputs a
  new binary file. 

  You need to change it so that it instead reads a list of 
  `.xyz` coordinate text files like `Unitymol_SVN/Assets/Resources/retal.xyz.txt`
  and outputs a binary `.moleculargeometry.bytes` file. Change the output file format
  so that it outputs the number of geometries in each direction  (nX and nY) as well. 

- Modify `Unitymol_SVN/Assets/Scripts/AtomMover.cs`. Make the GeometryData loaded
  from the Unity editor rather than from the script

- Modify `Unitymol_SVN/Assets/Scripts/AtomMover.cs`. Instead of moving one atom
  when the "j" key is held down, load a new geometry from the GeometryData 
  textasset. When the whole game is stictched together, this will be the motion
  of the golf ball.


Task 5 - Distribution and building
===================================
- Move the Unitymol_SVN Assets tree *inside* the `unityproject` assets tree. Then 
  modify all the paths to point at their new locations. Test the game.

- Add the unitymol_svn files to git. Make sure to exclude  temporary files
  like those listed in `unityproject/.gitignore`



Task 6 - User interface (James)
================================
"Sprite": A 2d object (like a cardboard cutout) in a 3d world that automatically
         turns to face the camera  all the time. 

- get the HUD working with a power bar. Make a direction arrow sprite. Put dashed coloured
  lines on the surface at the location of the ball showing the reaction coordinate values.
  Make a hovering arrow sprite showing where the hole is.

- Make helpful hints come up on the screen showing which keys to press. You 
  will need to use the `Rect`  class of Unity.

Task 7 - Menu screens and key bindings 
===============================================
- make the arrow keys work in the menu screens. Allow selection of the two differnt
  molecules.

- In "Project settings -> Input" add all the key bindings. Call them helpful 
  names like "Fire1" etc. There should be buttons to do the following:
    
  World mode:
  + move the camera
  + Stop the ball's motion
  + Centre the camera on the ball

  Putting mode:
  + Rotate the camera.
  + Putt the ball with a particular velocity

  Menu mode:
  + Move between menu items.
  + Select menu items

  Fullscreen molecule mode:
  + Rotate the molecule
  + zoom in and out
  
  There should also be buttons to switch between the modes. Make SetMode() and 
  GetMode() functions.


Task 8 - Wii remote bindings (Max)
===================================
- Load the accelerometer data in real time into Unity. Switch this off when the
  trigger is not pressed. 

- When a swing is being taken, find the maximum (or average) velocity. Scale this on a scale 
  from 0 to 1, with a modifier such that small children will easily be able to 
  putt the ball.


Later
########

 Poster / presentation (Papa Tim)
===============================
Make a poster explaining the science of enzymes and why it is relevant to everyday life....
And why it is important to research

Potential energy surfface (Tim)
================================
Make a potential energy surface in CHARMM.


Diffculty balancing
=====================
Make a mode for primary school children and another harder mode for secondary 
school



Optimization
==============
Only do this if the memory requirements become a problem.

Add a zoning system (`getzone, loadzone, unloadzone`) so that
we dont have to store all the geometries in memory at once. Find out how to reassign 
memory pointers in C# so that we can copy pointers to textassets. Can we have an
array of textassets? if so how do we move them around without copying data?
Make them load asynchronously. If the molecule hasnt finished loading, then simply
dont update the display. 

void GetNeighbours(int ZoneIDx, int ZoneIDy){
}

void LoadZone(Vector2 point){
  zoneIDx = point.x  ;
  nX
  
  GeomIDx = ;
  point.x;
  point.y;
  
  currentZoneGeometries = ;

  ZoneCache = ; // a List of textassets with the current zone at index zero, 
                // and the neighbouring zones in an anticlockwise fashion following.

}
