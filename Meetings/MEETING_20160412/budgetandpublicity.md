
Golf game budget possibilities
===============================

OK if we got given £5000 to spend on public engagement what would we do
with it? (Even if we don't get granted any money it's still fun to
play the "what if" game.)

## Equipment
Virtual reality headset, t shirts, posters

## Travel
Travel for 5 of us to take the golf game to another country. Eg
- A deprived area in Scotland / croatia
- A european science education festival (eg 
  http://www.science-on-stage.eu/page/display/4/88/0/festival-2017)

## Professional web design
A web page or app explaining the science behind the golf game in a 
visual way. Dom says it would be a better idea to save the money and do this
ourselves (eg Hannah).

These are the only things Tim could think of off the top of his head. Can you think of any other wacky ideas? Add them.

Creating the golf game website
================================
The golf game website should be ready by June. The website will contain 
the "alpha" version of the Golf Game (Version 0.2, which is already made),
 and an interactive website showing how it relates to science.

When the final version of the game is completed (late June) this will go
on there as well along with a score-board.

Publicising the golf game website
===================================
Once the website is created, we will try to get a link to it put on 
the front page of the following websites:
- Steam Indie Games 
- Oxford Sparks
- Royal Society of Chemistry website
- BIG science communication newsletter
- British Science Association website

Awards and grants
====================
We will apply for the following grants (if the TMCS budget is not 
sufficient) :

- EPSRC Impact Acceleration Public Engagement Award (£5000) 
  http://www.bristol.ac.uk/red/industry/impact-acceleration/
  See the `bristolpublicengagementawards` folder

- RSC outreach fund award £2000
  http://www.rsc.org/awards-funding/funding/outreach-fund/

Prizes
========
We will nominate the golf game project for the following prizes:

- RSC prize for public engagement (TODO: look up)

Golf game budget tasks 
==========================
## Dom
- Look up the possibility of taking the game to a science festival in Croatia
- Talk to Mark Wilson about the budget and whether we should apply for the
  grants listed above.
- Look up the possibility of putting the game on Steam indie games website

## Hannah
- oxford sparks website, rsc website. Read through the EPSRC Impact
  Acceleration Public Engagement Award guidelines



