# Cheltenham Master Plan #

### Lunches ###

Volunteers do not need to bring lunches to the festival. From an email from Steve:

> "great sandwich shop opposite festival site plus a volunteers room 
> stacked with tea coffee toast fruit etc."

Keep the receipts for everything you buy from the moment you leave your house
to the moment you return, and reclaim them.

### Location ###

The marquee is located at 

    109-111 Bath Rd, Cheltenham, Gloucestershire GL53 7LS

### Contacts ###

Tim Wiles: 07791 355 394

## Monday 6 June ##

- **Blond Tim** buys spare Wii remotes, kensington locks for laptops, plastic boxes
  for transporting kit

- **Max** pushes his golf game changes to the repository so debugging can commence.
  Put it in a seperate branch if it is not ready to be merged.

- **Jonathan and James** install Unity 5.3.5 on their laptops and get the game 
  compiled. 

- **Dan** Sorts the shirts out (and deliver them to one of the Friday volunteers (Dom?)). 

## Tues 7 June - Wed 8 June ##

- **Jonathan and James** fix all the bugs in the golf game 
  (specifically these ones: https://gitlab.com/tmcs/golfgame/milestones/7).
  Including the camera
  moving outside the map (https://gitlab.com/tmcs/golfgame/issues/82)
  and the ugly fonts (https://gitlab.com/tmcs/golfgame/issues/48).

- **Max** continues with his cracking Wii stuff

- **Papa Tim and Hannah** book hotels and arrange times and paperwork (after working
  out what day James is going)

- **Dan and Dom** install Unity 5.3.5 on their laptops and get the game 
  compiled (ask James and Jonathan for help). 

## Thurs 9 June ##

Equipment needed: 

- **Blond Tim**: Spare Wii remotes, memory sticks with the latest game version (assume
  there is no internet), kensington locks, plastic boxes for the kit

- **James, Dom**: Macbooks with Unity 5.3.5 and a recent (>=0.6) version of the game
  compiled and tested.

- **Dom**: T-shirts from Dan.

- **Papa Tim**: Paperwork, decorations for the stall (eg astro-turf and posters),
   a Mac-to-VGA or Mac-to-HDMI adaptor and cable.

- **Steve Dorney**: Giant screen.

- **Hannah**: A camera to photograph the event (this is probably the most important
  part of our outreach work).

Timings

- 15:00 DEADLINE for golf game completion. **Blond Tim** pulls latest master branch
  at 15:15, compiles it for Mac and Linux, and puts the compiled version on memory sticks in a
  sealed, waterproof bag.

- 18:30 **Papa Tim, Blond Tim, Dom, James**  meet 
  in Cheltenham for dinner and a brief evening stroll
  before going to the apartment and sleeping early. Blond Tim has the accommodation details.

## Fri 10 June ##

Timings:

- 08:00 **Papa Tim, Blond Tim, Dom, James** get in a taxi from apartment
  to the marquee to set up.

- 10:00 **Hannah** arrives on the shuttle bus from Southampton. 

- 16:30 After a day
  of publicly proclaiming their passion for potential-energy-surfaces,
  the **Friday volunteers** set up 
  things ready for the Saturday, then leave  
  Cheltenham and go home (apart from Papa Tim who is doing both days). 
  
  - The batteries for the Wii remotes need to be put on charge overnight
  - The screen needs to be moved out of the Marquee for security

- Evening: **Jonathan** arrives in Cheltenham.

## Sat 11 June ##

Equipment needed:

- **Dan, Jonathan**: Macbooks with Unity 5.3.5 and a recent (>=0.6) version of the game
  compiled and tested.


Timings:

- 09:00 **Papa Tim and Jonathan** go to the festival and begin
  spreading the joy of science (preferably both with laptops).

- 11:00 **Dan** arrives around this time with his laptop and joins in with the
  excitement

- 16:30 **Saturday volunteers** pack up and leave,
  possibly taking a taxi back to Oxford. Thank Steve and Tony for their help.

## Sun 12 June ##

(The sooner we do it the 
more fresh it will be in our minds.)

**Volunteer needed** to write an article on what we did, building around
the pictures Hannah took on the Friday. This is 
perhaps the most important part of our outreach work (providing evidence of what
we have done). 


