namespace ChemGolf{

using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using System;
using System.IO;
using System.Text.RegularExpressions;


public class MoleculeLoader : MonoBehaviour {

  public int moleculeLayerID  = 10;
  public Material[] materials ;
  public float[] radii ;
  public string[] elementSymbols;
  public TextAsset atomsFile;
  public TextAsset bondsFile;

  void Start(){
    LoadMolecule(GetComponent<MoleculeMover>());
    Destroy(this); // Remove the molecule loader once it has fulfilled its function
  }

  // ################################################
  // ################################################
  // ################################################
  

  public GameObject CreateAtom(int elementindex){
		GameObject o = GameObject.CreatePrimitive(PrimitiveType.Sphere);
    o.transform.parent = transform;
    o.transform.localScale = new Vector3(2f*radii[elementindex],2f*radii[elementindex], 2f*radii[elementindex]);
    o.GetComponent<Renderer>().material = materials[elementindex];
    o.layer = moleculeLayerID;
    return o;
  }


  // ################################################
  // ################################################
  // ################################################

  public void LoadMolecule(MoleculeMover moleculeMover) { 

    // --------------- create the atoms --------------------------
    int numAtoms;
    using (StringReader reader = new StringReader(atomsFile.text)){
      numAtoms = Int32.Parse(reader.ReadLine());
      moleculeMover.atoms = new Transform[numAtoms];
      for (int i=0; i<numAtoms; i++){
        string atomtype = reader.ReadLine();
        Assert.IsNotNull(atomtype); // TODO is this line necessary? test by putting in too few atoms
        int elementindex = Array.IndexOf( elementSymbols, atomtype );
        if (elementindex==-1){ 
          throw new Exception("Unknown element: "+ atomtype);
        }
        moleculeMover.atoms[i] = CreateAtom(elementindex).transform;
      }
    }
    // ----------- end create the atoms --------------------------


    // --------------- create the bonds --------------------------------
    int numBonds;
    Regex r = new Regex( @"(\d+)\s+(\d+)\s+(\w+)");  
            
    using (StringReader reader = new StringReader(bondsFile.text)){
      numBonds = Int32.Parse(reader.ReadLine());
      moleculeMover.bonds = new Bond[numBonds];
      moleculeMover.bondStarts = new int[numBonds];
      moleculeMover.bondEnds = new int[numBonds];
      for (int i=0; i<numBonds; i++){
        string line = reader.ReadLine();
        Assert.IsNotNull(line); // TODO is this line necessary? test by putting in too few atoms
        Match m = r.Match(line);
        Assert.IsTrue(m.Success);
        moleculeMover.bondStarts[i] = Int32.Parse(m.Groups[1].Value);
        moleculeMover.bondEnds[i] = Int32.Parse(m.Groups[2].Value);
        string bondType = m.Groups[3].Value;
        GameObject o;
        switch (bondType){
          case "single":
            o = TubeBond.CreateBond();
            o.transform.parent = transform;
            moleculeMover.bonds[i] = o.GetComponent<Bond>(); 
            break;
          default:
            throw new Exception("Unknow bond type "+bondType);
        }
        o.transform.parent = transform;
        o.layer = moleculeLayerID;
      }
    }
    // ----------- end create the bonds --------------------------------

  }


}



} // namespace ChemGolf


