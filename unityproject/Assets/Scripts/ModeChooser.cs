using UnityEngine;
using UnityEngine.UI;

namespace ChemGolf{

public enum ViewMode{  WholeMapView, PuttView, WholeMolecule };

public class ModeChooser: MonoBehaviour {

    private ViewMode viewMode_;

    public Canvas wholeMoleculeUI;
    public GolfBall golfBall;
    public WiiMoteInputManager wiiManager;
    public float wholeWorldCameraSwitchDelay = 0.8f; // switch to the whole world camera x seconds after putting
    public GameObject wholeWorldCamera; // the camera that shows the whole world
    public GameObject moleculeCamera; // the camera that shows the molecule
    public GameObject fullscreenMoleculeCamera; 
    public GameObject puttingCamera;

// ######################################################################
// ######################################################################
// ######################################################################


  public ViewMode viewMode{
    get{
      return viewMode_;
    } set {
      viewMode_ = value;
      string switchhint = "\nA button / Q key: Switch";
      // --------------- Set the active camera -----------------------------
      switch (value) {
        case ViewMode.WholeMapView:
          wholeWorldCamera.SetActive(true);
          puttingCamera.SetActive(false);
          fullscreenMoleculeCamera.SetActive(false);
          moleculeCamera.SetActive(true);
          wholeMoleculeUI.gameObject.SetActive(false);
          wholeWorldCamera.GetComponent<WholeWorldCamera>().CentreOnBall();
          GetComponent<Text>().text = "Map view"+switchhint;
          golfBall.powerbar.gameObject.SetActive(false);
          break;  
        case ViewMode.PuttView:
          wholeWorldCamera.SetActive(false);
          puttingCamera.SetActive(true);
          fullscreenMoleculeCamera.SetActive(false);
          moleculeCamera.SetActive(true);
          wholeMoleculeUI.gameObject.SetActive(false);
          GetComponent<Text>().text = "Putt view"+switchhint;
          golfBall.powerbar.gameObject.SetActive(false);
          break;
        case ViewMode.WholeMolecule:
          wholeWorldCamera.SetActive(false);
          puttingCamera.SetActive(false);
          fullscreenMoleculeCamera.SetActive(true);
          moleculeCamera.SetActive(false);
          wholeMoleculeUI.gameObject.SetActive(true);
          GetComponent<Text>().text = "Molecule view"+switchhint;
          golfBall.powerbar.gameObject.SetActive(false);
          break;
      }
    }
  }

// ######################################################################
// ######################################################################
// ######################################################################

public void SwitchView(){
  switch (viewMode) {
    case ViewMode.WholeMapView:
      viewMode = ViewMode.PuttView;
      break;
    case ViewMode.PuttView:
      viewMode = ViewMode.WholeMolecule;
      break;
    case ViewMode.WholeMolecule:
      viewMode = ViewMode.WholeMapView;
      break;
  }
}

// ######################################################################
// ######################################################################
// ######################################################################


  void Update(){

    if (Input.GetButtonDown("switchview") || wiiManager.isPressed(WiiMoteInputManager.buttons.a) ){ 
      // switch to whole world view and back
      SwitchView();
    }

    // --------------- switch back to world view after putting----------------
    if (golfBall.moveMode==MoveMode.Putting){
      if ( golfBall.timeLastPutted + wholeWorldCameraSwitchDelay <  Time.time  ) {
        viewMode = ViewMode.WholeMapView;
      }
    }
    // ----------- end switch back to world view after putting----------------

  }


} // ModeChooser


} //namespace ChemGolf
