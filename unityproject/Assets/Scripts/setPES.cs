﻿namespace ChemGolf{

using UnityEngine;
using System.IO;
using System;
using System.Collections;
using UnityEngine.Assertions;


public class setPES : MonoBehaviour {
  //Public Variables
  public float heightscale=0.1f;
  public TextAsset energiesFile;

  void Start() {
    CreateSurface();
  }


  
  /// <summary>
  /// Returns the size of the header in bytes.
  /// </summary>
  public long ParseEnergiesHeader(out int nX, out int nY){

    Stream s = new MemoryStream(energiesFile.bytes);

    using (BinaryReader reader = new BinaryReader(s)){
      nX = reader.ReadInt32();
      nY = reader.ReadInt32(); 

      return s.Position; 

    }    
 
  }

  // ######################################################################
  // ######################################################################
  // ######################################################################
  

  private void CreateSurface(){
    int width = GetComponent<Terrain>().terrainData.heightmapWidth;
    int height = GetComponent<Terrain>().terrainData.heightmapHeight;
    float[,] pesHeights = GetComponent<Terrain>().terrainData.GetHeights (0, 0, width, height);

    int nX, nY;
    long headersize;

    headersize = ParseEnergiesHeader(out  nX, out  nY);

    Assert.AreEqual(nX, width); // THis should fail at the moment
    Assert.AreEqual(nY, height);

    Stream s = new MemoryStream(energiesFile.bytes);
    s.Seek(headersize, SeekOrigin.Current);
    
    float minval = 0.0f;
    using (BinaryReader reader = new BinaryReader(s)){
      for (int z_point = 0; z_point < height; z_point++) {
        for (int x_point = 0; x_point < width; x_point++) {
          pesHeights[x_point,z_point] = reader.ReadSingle()*heightscale;
          minval = Mathf.Min(minval,pesHeights[x_point,z_point]);
        }
      }
    }
    Assert.IsTrue(minval>=0.0f, "All energy values should be >=0. Found value "+minval);

	  GetComponent<Terrain>().terrainData.SetHeights( 0, 0, pesHeights);


  }



}

} // namespace ChemGolf
