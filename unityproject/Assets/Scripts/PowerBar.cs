namespace ChemGolf{

using UnityEngine;
using UnityEngine.UI;

public class PowerBar: MonoBehaviour {

  private float power_;
  public float power {
    get{ 
      return power_;
    } set{
      power_ = value;
      GetComponent<Image>().rectTransform.offsetMax = 
        new Vector2(GetComponent<Image>().rectTransform.offsetMax.x, 
                    GetComponent<Image>().rectTransform.offsetMin.y + power_
                    );
    }
  }


} // PowerBar


} //namespace ChemGolf
