﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace ChemGolf{

public class WiiMoteInputManager : MonoBehaviour {

    private List<bool> pressed = new List<bool> {false,false,false,false,false,false};
    private List<bool> held= new List<bool> {false,false,false,false,false,false};
    private List<bool> released= new List<bool> {false,false,false,false,false,false};
    public enum buttons {a,b,d_left,d_right,d_up,d_down};

    public float wiiForceModifier=0.004f;
    public float wiiArrowSpeed = 1f;

	// Use this for initialization
	void Start () {
	    
	}

  /// Should return 0 for no swing, and 1 for the highest possible swing
  public float WiiSwingPower(){
    return Mathf.Min(1f, wiiForceModifier * 
          Mathf.Sqrt( GameManager.instance.wiimote.MotionPlus.YawSpeed
                     *GameManager.instance.wiimote.MotionPlus.YawSpeed 
                    + GameManager.instance.wiimote.MotionPlus.PitchSpeed
                     *GameManager.instance.wiimote.MotionPlus.PitchSpeed ));
  }  

  public float DpadGetAxis(string axis){
    float speed = 0;
    switch (axis) {
      case "Horizontal":
        if (isHeld(buttons.d_left)) speed -= wiiArrowSpeed ;        
        if (isHeld(buttons.d_right))speed += wiiArrowSpeed ;
        break;
      case "Vertical":
        if (isHeld(buttons.d_down)) speed -= wiiArrowSpeed ;        
        if (isHeld(buttons.d_up))   speed += wiiArrowSpeed ;
        break;
      default:
        throw new Exception ("Unknown D-pad axis requested: " + axis);
    }
    return speed;
  }


	// Update is called once per frame
	void FixedUpdate () {
    if (GameManager.instance.wiimote!=null){
      checka();
      checkb();
      checkLeft();
      checkRight();
      checkUp();
      checkDown();
    }
	}

  public bool isPressed(buttons e)
    {
      return pressed[(int)e];
    }

  public bool isHeld(buttons e)
    {
      return held[(int)e];
    }

  public bool isReleased(buttons e)
    {
      return released[(int)e];
    }

  public void toggleReleased(buttons e)
    {
     released[(int)e] = false;
    }

    private void checkb()
  {
      if (GameManager.instance.wiimote.Button.b && !pressed[(int)buttons.b] && !held[(int)buttons.b])
        {
                  Debug.Log("Been Pressed");
          pressed[(int)buttons.b] = true;
          held[(int)buttons.b] = false;
          released[(int)buttons.b] = false;
      } else if (GameManager.instance.wiimote.Button.b && pressed[(int)buttons.b] && !held[(int)buttons.b] && !released[(int)buttons.b])
        {
                    Debug.Log("Been Held");
          pressed[(int)buttons.b] = false;
          held[(int)buttons.b] = true;
          released[(int)buttons.b] = false;
        }

      if (!GameManager.instance.wiimote.Button.b && !pressed[(int)buttons.b] && held[(int)buttons.b] && !released[(int)buttons.b])
        {
          Debug.Log("Been Released");
          pressed[(int)buttons.b] = false;
          held[(int)buttons.b] = false;
          released[(int)buttons.b] = true;
        }
  }

  private void checka()
    {
      if (GameManager.instance.wiimote.Button.a && !pressed[(int)buttons.a] && !held[(int)buttons.a] && !released[(int)buttons.a])
        {
          //        Debug.Log("Been Pressed");
          pressed[(int)buttons.a] = true;
          held[(int)buttons.a] = false;
          released[(int)buttons.a] = false;
      } else if (GameManager.instance.wiimote.Button.a && pressed[(int)buttons.a] && !held[(int)buttons.a] && !released[(int)buttons.a])
        {
          //          Debug.Log("Been Held");
          pressed[(int)buttons.a] = false;
          held[(int)buttons.a] = true;
          released[(int)buttons.a] = false;
        }

      if (!GameManager.instance.wiimote.Button.a && !pressed[(int)buttons.a] && held[(int)buttons.a] && !released[(int)buttons.a])
        {
          //          Debug.Log("Been Released");
          pressed[(int)buttons.a] = false;
          held[(int)buttons.a] = false;
          released[(int)buttons.a] = true;
        }

      if (!GameManager.instance.wiimote.Button.a && !pressed[(int)buttons.a] && !held[(int)buttons.a] && released[(int)buttons.a])
        {
          //          Debug.Log("Resetting");
          pressed[(int)buttons.a] = false;
          held[(int)buttons.a] = false;
          released[(int)buttons.a] = false;
        }
    }

  private void checkLeft()
    {
      if (GameManager.instance.wiimote.Button.d_left && !pressed[(int)buttons.d_left] && !held[(int)buttons.d_left] && !released[(int)buttons.d_left])
        {
          //        Debug.Log("Been Pressed");
          pressed[(int)buttons.d_left] = true;
          held[(int)buttons.d_left] = false;
          released[(int)buttons.d_left] = false;
      } else if (GameManager.instance.wiimote.Button.d_left && pressed[(int)buttons.d_left] && !held[(int)buttons.d_left] && !released[(int)buttons.d_left])
        {
          //          Debug.Log("Been Held");
          pressed[(int)buttons.d_left] = false;
          held[(int)buttons.d_left] = true;
          released[(int)buttons.d_left] = false;
        }

      if (!GameManager.instance.wiimote.Button.d_left && !pressed[(int)buttons.d_left] && held[(int)buttons.d_left] && !released[(int)buttons.d_left])
        {
          //          Debug.Log("Been Released");
          pressed[(int)buttons.d_left] = false;
          held[(int)buttons.d_left] = false;
          released[(int)buttons.d_left] = true;
        }

      if (!GameManager.instance.wiimote.Button.d_left && !pressed[(int)buttons.d_left] && !held[(int)buttons.d_left] && released[(int)buttons.d_left])
        {
          //          Debug.Log("Resetting");
          pressed[(int)buttons.d_left] = false;
          held[(int)buttons.d_left] = false;
          released[(int)buttons.d_left] = false;
        }
    }

  private void checkRight()
    {
      if (GameManager.instance.wiimote.Button.d_right && !pressed[(int)buttons.d_right] && !held[(int)buttons.d_right] && !released[(int)buttons.d_right])
        {
          //        Debug.Log("Been Pressed");
          pressed[(int)buttons.d_right] = true;
          held[(int)buttons.d_right] = false;
          released[(int)buttons.d_right] = false;
      } else if (GameManager.instance.wiimote.Button.d_right && pressed[(int)buttons.d_right] && !held[(int)buttons.d_right] && !released[(int)buttons.d_right])
        {
          //          Debug.Log("Been Held");
          pressed[(int)buttons.d_right] = false;
          held[(int)buttons.d_right] = true;
          released[(int)buttons.d_right] = false;
        }

      if (!GameManager.instance.wiimote.Button.d_right && !pressed[(int)buttons.d_right] && held[(int)buttons.d_right] && !released[(int)buttons.d_right])
        {
          //          Debug.Log("Been Released");
          pressed[(int)buttons.d_right] = false;
          held[(int)buttons.d_right] = false;
          released[(int)buttons.d_right] = true;
        }

      if (!GameManager.instance.wiimote.Button.d_right && !pressed[(int)buttons.d_right] && !held[(int)buttons.d_right] && released[(int)buttons.d_right])
        {
          //          Debug.Log("Resetting");
          pressed[(int)buttons.d_right] = false;
          held[(int)buttons.d_right] = false;
          released[(int)buttons.d_right] = false;
        }
    }

  private void checkUp()
    {
      if (GameManager.instance.wiimote.Button.d_up && !pressed[(int)buttons.d_up] && !held[(int)buttons.d_up] && !released[(int)buttons.d_up])
        {
          //        Debug.Log("Been Pressed");
          pressed[(int)buttons.d_up] = true;
          held[(int)buttons.d_up] = false;
          released[(int)buttons.d_up] = false;
      } else if (GameManager.instance.wiimote.Button.d_up && pressed[(int)buttons.d_up] && !held[(int)buttons.d_up] && !released[(int)buttons.d_up])
        {
          //          Debug.Log("Been Held");
          pressed[(int)buttons.d_up] = false;
          held[(int)buttons.d_up] = true;
          released[(int)buttons.d_up] = false;
        }

      if (!GameManager.instance.wiimote.Button.d_up && !pressed[(int)buttons.d_up] && held[(int)buttons.d_up] && !released[(int)buttons.d_up])
        {
          //          Debug.Log("Been Released");
          pressed[(int)buttons.d_up] = false;
          held[(int)buttons.d_up] = false;
          released[(int)buttons.d_up] = true;
        }

      if (!GameManager.instance.wiimote.Button.d_up && !pressed[(int)buttons.d_up] && !held[(int)buttons.d_up] && released[(int)buttons.d_up])
        {
          //          Debug.Log("Resetting");
          pressed[(int)buttons.d_up] = false;
          held[(int)buttons.d_up] = false;
          released[(int)buttons.d_up] = false;
        }
    }

  private void checkDown()
    {
      if (GameManager.instance.wiimote.Button.d_down && !pressed[(int)buttons.d_down] && !held[(int)buttons.d_down] && !released[(int)buttons.d_down])
        {
          //        Debug.Log("Been Pressed");
          pressed[(int)buttons.d_down] = true;
          held[(int)buttons.d_down] = false;
          released[(int)buttons.d_down] = false;
      } else if (GameManager.instance.wiimote.Button.d_down && pressed[(int)buttons.d_down] && !held[(int)buttons.d_down] && !released[(int)buttons.d_down])
        {
          //          Debug.Log("Been Held");
          pressed[(int)buttons.d_down] = false;
          held[(int)buttons.d_down] = true;
          released[(int)buttons.d_down] = false;
        }

      if (!GameManager.instance.wiimote.Button.d_down && !pressed[(int)buttons.d_down] && held[(int)buttons.d_down] && !released[(int)buttons.d_down])
        {
          //          Debug.Log("Been Released");
          pressed[(int)buttons.d_down] = false;
          held[(int)buttons.d_down] = false;
          released[(int)buttons.d_down] = true;
        }

      if (!GameManager.instance.wiimote.Button.d_down && !pressed[(int)buttons.d_down] && !held[(int)buttons.d_down] && released[(int)buttons.d_down])
        {
          //          Debug.Log("Resetting");
          pressed[(int)buttons.d_down] = false;
          held[(int)buttons.d_down] = false;
          released[(int)buttons.d_down] = false;
        }
    }

}
} // namespace ChemGolf
