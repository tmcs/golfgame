﻿
namespace ChemGolf{

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Collections;
using System;


public class Temperature : MonoBehaviour {

  public WiiMoteInputManager wiiManager;
  public Image thermometer;
  public float temperature=2f;
  public float maxTemperature =6f;
  private float barScale = 20f;
  public float tempChangeSpeed =1f;

  void Start(){
    Image image = GetComponent<Image>();
    float maxBarHeight = image.rectTransform.offsetMax.y - image.rectTransform.offsetMin.y;
    Assert.IsTrue(maxBarHeight>0);
    barScale = maxBarHeight / maxTemperature  ; 
  }

  void Update(){
    Image image = GetComponent<Image>();
    image.rectTransform.offsetMax=new Vector2(image.rectTransform.offsetMax.x, 
                image.rectTransform.offsetMin.y + temperature*barScale);
  }
  
  void FixedUpdate(){
    temperature += (wiiManager.DpadGetAxis("Vertical") + Input.GetAxis("Vertical") ) 
                      * tempChangeSpeed;
    temperature = Mathf.Min(temperature, maxTemperature);
    temperature = Mathf.Max(temperature, 0f);
  }


}

} // namespace ChemGolf
