﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.EventSystems;

public class MenuScript : MonoBehaviour {

	public Canvas levelMenu;
	public Canvas quitMenu;
	public Button levelText;
	public Button quitText;
	public static int scene;
    public GameObject level1button;
    public GameObject levelsbutton;
    public GameObject quitnobutton;

	// Use this for initialization
	void Start ()
	{
		levelMenu = levelMenu.GetComponent<Canvas>();
		quitMenu = quitMenu.GetComponent<Canvas>();
		levelText = levelText.GetComponent<Button>();
		quitText = quitText.GetComponent<Button>();
		levelMenu.enabled = false;
		quitMenu.enabled = false;
		scene = SceneManager.GetActiveScene().buildIndex;
	}


  void Update(){
    if (Input.GetButtonDown("Cancel")){
      if (levelMenu.enabled){
        BackPress();
      } else if(quitMenu.enabled){
        NoPress();
      } else{
        ExitPress();
      }
    }
  }

	public void LevelPress()
	{
		levelMenu.enabled = true;
		levelText.enabled = false;
		quitText.enabled = false;
        EventSystem es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        es.SetSelectedGameObject(level1button);
	}

	public void ExitPress()
	{
		quitMenu.enabled = true;
		levelText.enabled = false;
		quitText.enabled = false;
        EventSystem es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        es.SetSelectedGameObject(quitnobutton);
	}

	public void NoPress()
	{
		quitMenu.enabled = false;
		levelText.enabled = true;
		quitText.enabled = true;
        EventSystem es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        es.SetSelectedGameObject(levelsbutton);
	}

	public void BackPress()
	{
		levelMenu.enabled = false;
		levelText.enabled = true;
		quitText.enabled = true;
        EventSystem es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        es.SetSelectedGameObject(levelsbutton);
	}

	public void StartLevel1()
	{
//		SceneManager.UnloadScene ("MenuScene");
//		SceneManager.LoadScene ("Testlevel");
      GameManager.instance.ChangeLevel("Testlevel");
	}

	public void StartLevel2()
	{
		SceneManager.UnloadScene ("MenuScene");
		SceneManager.LoadScene (2);
	}

	public void QuitGame()
	{
      #if UNITY_EDITOR
      Debug.Log("Game has been quit");
      #else
      Application.Quit();
      #endif 
	}

}
