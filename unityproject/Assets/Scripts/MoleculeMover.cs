namespace ChemGolf{


using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using System.IO;
using System;
using System.Text.RegularExpressions;

  // ################################################
  // ################################################
  // ################################################

public struct SquareIndex{
  public int X;
  public int Y;

  public SquareIndex(int Xin, int Yin){
    X = Xin;
    Y = Yin;
  }

  public override int GetHashCode(){
    // Thanks to http://stackoverflow.com/a/13812527 for this one.
    return (X << 5) + X ^ Y;
  }

  public override bool Equals(object obj){
    if (obj == null || GetType() != obj.GetType()) return false;
    return Equals(obj);
  }

  public bool Equals(SquareIndex c2){
    return X==c2.X && Y==c2.Y;
  }

  
  public static bool operator ==(SquareIndex c1, SquareIndex c2) 
  {
    return c1.Equals(c2);
  }

  public static bool operator !=(SquareIndex c1, SquareIndex c2) 
  {
    return ! c1.Equals(c2);
  }

}

  // ################################################
  // ################################################
  // ################################################

public class MoleculeMover : MonoBehaviour {

  public Transform[] atoms;
  public Bond[] bonds;
  public int[] bondStarts;
  public int[] bondEnds;
  public TextAsset atomPositionsFile; 
  public TextAsset bondVisibilityFile;
  public GolfBall gb;

  private SquareIndex currentSquare_  = new SquareIndex(-1,-1);

  void Start(){
    currentSquare_.X  = -1; // Ensures molecule is reloaded
    currentSquare_.Y  = -1;
  }

  // ################################################
  // ################################################
  // ################################################

	void Update () {

    currentSquare = GetSquareIndex(gb);

  }

  // ################################################
  // ################################################
  // ################################################
 
  public SquareIndex currentSquare {
    get{
      return currentSquare_;
    }

    set{

      if (atoms!=null && value!=currentSquare_){  // if the molecule is loaded
        currentSquare_ = value;
        CheckPESSizesMatch();
        UpdatePositions();
        UpdateBondVisibilities();
        UpdateBonds();
      }
    }
  }


// ######################################################################
// ######################################################################
// ######################################################################

  public SquareIndex GetSquareIndex(Vector2 fractionalPosition){
    SquareIndex squareIndex ;
    int nX, nY;
    ParseAtomPositionHeader(out  nX, out  nY);
    squareIndex.X = (int) (fractionalPosition[0] * (float) nX);
    squareIndex.Y = (int) (fractionalPosition[1] * (float) nY);
    return squareIndex;
  }

  public SquareIndex GetSquareIndex(GolfBall gb){
    return GetSquareIndex(gb.GetFractionalPosition());
  }


  // ################################################
  // ################################################
  // ################################################
  void UpdatePositions(){
    // --------------- parse headers ------------------------------------
    int nX, nY;
    long headersize;

    headersize = ParseAtomPositionHeader(out  nX, out  nY);
    Stream atomPositionsStream = new MemoryStream(atomPositionsFile.bytes);
    atomPositionsStream.Seek(headersize, SeekOrigin.Current);
    // ----------- end parse headers ------------------------------------

    // --------------- get atom positions -------------------------------
    const long sizeofFloat32 = 4; // in bytes
    long atomPositionsStreamsizeofsquare = atoms.Length*3*sizeofFloat32;
    atomPositionsStream.Seek( ( currentSquare_.X*nY + currentSquare_.Y )*atomPositionsStreamsizeofsquare , 
                                                  SeekOrigin.Current);
    Vector3 atomPos = new Vector3();
    using (BinaryReader reader = new BinaryReader(atomPositionsStream)){
      for (int i=0; i<atoms.Length; i++){
        for (int j=0; j<3; j++){
          atomPos[j] = reader.ReadSingle();
        }
        atoms[i].localPosition = atomPos; // TODO check this copies
      }
    }
    // ----------- end get atom positions -------------------------------


  }

  // ######################################################################
  // ######################################################################
  // ######################################################################
  
  void UpdateBondVisibilities(){

    // --------------- parse headers ------------------------------------
    int nX, nY;
    long headersize = ParseBondVisibilityHeader(out  nX, out  nY);

    Stream bondVisibilityStream = new MemoryStream(bondVisibilityFile.bytes);
    bondVisibilityStream.Seek(headersize, SeekOrigin.Current);
    // ----------- end parse headers ------------------------------------


    // --------------- get bond visibilities ---------------------------
    long bondVisibilityStreamsizeofsquare = bonds.Length*1;
    bondVisibilityStream.Seek( ( currentSquare_.X*nY + currentSquare_.Y )*bondVisibilityStreamsizeofsquare , 
                                                    SeekOrigin.Current);

    using (BinaryReader reader = new BinaryReader(bondVisibilityStream)){
      for (int i=0; i<bonds.Length; i++){
        bonds[i].SetVisible(reader.ReadBoolean());
      }
    }
    // ----------- end get bond visibilities ---------------------------

  }


  // ################################################
  // ################################################
  // ################################################

  void CheckPESSizesMatch(){
    int nX, nY;
    ParseAtomPositionHeader(out  nX, out  nY);
    int nX2, nY2;    
    ParseBondVisibilityHeader(out  nX2, out  nY2);
    Assert.AreEqual(nX, nX2);
    Assert.AreEqual(nY, nY2);
   

    // --------------- check square index ---------------------------------
    if (currentSquare_.X >= nX||currentSquare_.Y>= nY) throw new IndexOutOfRangeException("Geometry "
                          +currentSquare_.X+","+currentSquare_.Y+" requested but size is "+nX+","+nY);
    // ----------- end check square index ---------------------------------
 
  }


  // ######################################################################
  // ######################################################################
  // ######################################################################  

  /// <summary>
  /// Returns the size of the header in bytes.
  /// </summary>
  public long ParseAtomPositionHeader(out int nX, out int nY){

    Stream atomPositionsStream = new MemoryStream(atomPositionsFile.bytes);

    using (BinaryReader reader = new BinaryReader(atomPositionsStream)){
      int atomPositionsStreamnAtom  = reader.ReadInt32(); // The number of atoms.
      int atomPositionsStreamnX = reader.ReadInt32();
      int atomPositionsStreamnY = reader.ReadInt32(); 

      if (atomPositionsStreamnAtom != atoms.Length){
         throw new Exception ("atomPositions file has wrong number of atoms.");
      }

      nX = atomPositionsStreamnX;
      nY = atomPositionsStreamnY;

      return atomPositionsStream.Position;    // TODO does this return 0?

    }    



 
  }

  // ################################################
  // ################################################
  // ################################################
  
  /// <summary>
  /// Returns the size of the header in bytes.
  /// </summary>
  public long ParseBondVisibilityHeader(out int nX, out int nY){
    Stream bondVisibilityStream = new MemoryStream(bondVisibilityFile.bytes);

    using (BinaryReader reader = new BinaryReader(bondVisibilityStream)){
      int bondVisibilityStreamnBond  = reader.ReadInt32(); // The number of bonds.
      int bondVisibilityStreamnX = reader.ReadInt32();
      int bondVisibilityStreamnY = reader.ReadInt32(); 

      if (bondVisibilityStreamnBond != bonds.Length){
         throw new Exception ("bondVisibility file has wrong number of atoms.");
      }

      nX = bondVisibilityStreamnX;
      nY = bondVisibilityStreamnY;

      return bondVisibilityStream.Position;

    }    


  }


  // ################################################
  // ################################################
  // ################################################
  

  void UpdateBonds(){
    for (int i=0; i<bonds.Length; i++){
      bonds[i].MoveBond(atoms[bondStarts[i]].localPosition, atoms[bondEnds[i]].localPosition  );
    }
  }




}

} // namespace ChemGolf
