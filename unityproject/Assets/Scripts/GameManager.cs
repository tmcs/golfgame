﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using WiimoteApi;

using System.Collections.Generic;

public class GameManager : MonoBehaviour
{

  public static GameManager instance = null;
  private string level = "MenuScene";
  public Wiimote wiimote;

  void Awake()
    {
        if (instance == null) {
          instance = this;
        } else if (instance != this) {
         Debug.Log("More than one instance of GameManager Destroying Object");
          Destroy (gameObject);    
        }

      DontDestroyOnLoad(gameObject);

      StartGame();
    }

    void Start() {
//      WiimoteManager.Debug_Messages = true;
      connectWiiMote();
    }

  void StartGame()
    {
      SceneManager.LoadScene(level);
    }

    public void ChangeLevel(string l) {
      instance.level = l;
      SceneManager.LoadScene(l);
    }

    void OnApplicationQuit(){
        if (wiimote != null) {
          Debug.Log("Cleaning up WiiMotes");
          WiimoteManager.Cleanup(wiimote);
        }
  }

  void FixedUpdate ()
    {
      if (wiimote != null)
      {
          int ret;
          do
            {
              ret = wiimote.ReadWiimoteData();
            } while (ret > 0);
      }
    }

    public void connectWiiMote()
  {
      wiimote = null;
      WiimoteManager.FindWiimotes();
      if (WiimoteManager.HasWiimote())
        {
          wiimote = WiimoteManager.Wiimotes[0];
          wiimote.SendPlayerLED(true, false, false, false);

          // set output data format
          wiimote.SendDataReportMode(InputDataType.REPORT_BUTTONS_ACCEL_EXT16);

          wiimote.RequestIdentifyWiiMotionPlus();
          wiimote.RumbleOn = true; // Enabled Rumble
          wiimote.SendStatusInfoRequest(); // Requests Status Report, encodes Rumble into input report
          System.Threading.Thread.Sleep(1000); // Wait 1.0s
          wiimote.RumbleOn = false; // Disabled Rumble
          wiimote.SendStatusInfoRequest(); // Requests Status Report, encodes Rumble into input report
          wiimote.ActivateWiiMotionPlus();
        }
  }

    public void dissconnectWiiMote()
    {
    if (wiimote != null)
      {
          WiimoteManager.Cleanup(wiimote);
          wiimote = null;
      }
    }
     
}
