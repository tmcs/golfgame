
﻿﻿namespace ChemGolf{

using UnityEngine;
using System.Collections;

public class TubeBond: Bond {

  private float bondRadiusScale = 0.5f;

  public override void SetVisible(bool visible){
    GetComponent<MeshRenderer>().enabled = visible;
  }

  public static GameObject CreateBond(){ 
		GameObject o = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
    o.AddComponent<TubeBond>();
    return o;
  }


  public override void MoveBond(Vector3 startPos, Vector3 endPos){
      Vector3 bondVector = endPos - startPos;
      // TODO The following line is out by a constant. What is it?
      float bondRadius = bondRadiusScale* 
                1.0f/Mathf.Sqrt(bondVector.magnitude); // Keeps volume of tube constant
      transform.localScale = new Vector3(
                                       0.5f*bondRadius,
                                       0.5f*bondVector.magnitude, 
                                       0.5f*bondRadius
                                        );
      transform.localRotation = Quaternion.FromToRotation(Vector3.up, bondVector);
      transform.localPosition = startPos+ 0.5f*bondVector;

  }


}
} //namespace ChemGolf
