﻿using UnityEngine;
﻿using UnityEngine.UI;
using System.Collections;

namespace ChemGolf{

public class InputHints : MonoBehaviour{

  public GolfBall golfBall;

  void Update(){

    string puttingText = "";
    if (golfBall.modeChooser.viewMode==ViewMode.WholeMapView) {
      if (golfBall.moveMode==MoveMode.Putting){
        puttingText = "Wait for the ball to stop";
      } else if (golfBall.moveMode==MoveMode.Frozen) {
        puttingText = "Use the arrow keys to move around"; 
      }
    } else if (golfBall.modeChooser.viewMode==ViewMode.PuttView){
      if (golfBall.moveMode==MoveMode.Frozen){
        puttingText = "Hold the space bar / swing the Wii-mote to putt the ball"; 
      }
    } else if (golfBall.modeChooser.viewMode==ViewMode.WholeMolecule){
      puttingText = "Press Up/Down to change temperature."; 
    }

    GetComponent<Text>().text = puttingText;
  }

} // InputHints

} // namespace ChemGolf

