﻿
namespace ChemGolf{

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Collections;
using System;


public class KeepInCorner : MonoBehaviour {
  /// Distance from corner in units of screen size in each direction.
  public Vector2 margin = new Vector2(0.1f,0.1f); 
  public float distanceAway = 4f; // This affects the clipping of the molecule
  public Camera HUDCamera;


  void Update(){
    Vector3 cornerPos = new Vector3(margin.x, margin.y, distanceAway);
    transform.position = HUDCamera.ViewportToWorldPoint(cornerPos);

  }
  


}


} // namespace ChemGolf
