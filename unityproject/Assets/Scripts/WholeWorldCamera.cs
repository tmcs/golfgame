namespace ChemGolf{

using UnityEngine;

public class WholeWorldCamera: MonoBehaviour {

    public GolfBall golfBall;
    public WiiMoteInputManager wiiManager;
    public float wholeWorldCameraSpeedhorizontal=1f;
    public float wholeWorldCameraSpeedvertical=1f ;
    public float wholeWorldCameraMaxSpeed=10f;
    public float Xmargin=10f;
    public float Zmargin=10f;
    public Vector3 wholeWorldCameraOffset = new Vector3(0f, 20f, -20f );


    public LayerMask wholeWorldCameraZoomLayerMask;



// ######################################################################
// ######################################################################
// ######################################################################



  void FixedUpdate(){

    switch (golfBall.moveMode) {
      case MoveMode.Frozen: 
        Vector3 pos = transform.position - wholeWorldCameraOffset;
        pos.x += (wiiManager.DpadGetAxis("Horizontal") + Input.GetAxis("Horizontal"))
                           * wholeWorldCameraSpeedhorizontal;
        pos.x = Mathf.Min( pos.x, golfBall.Xmax()-Xmargin );
        pos.x = Mathf.Max( pos.x, golfBall.Xmin()+Xmargin );
        pos.z += (wiiManager.DpadGetAxis("Vertical") + Input.GetAxis("Vertical") )
                           * wholeWorldCameraSpeedvertical;
        pos.z = Mathf.Min( pos.z, golfBall.Zmax()-Zmargin );
        pos.z = Mathf.Max( pos.z, golfBall.Zmin()+Zmargin );
        transform.position = pos + wholeWorldCameraOffset;
        break;
      case MoveMode.ManualForce:
      case MoveMode.Putting:
        CentreOnBall();
        break;
    }

  }


    // ################################################
    // ################################################
    // ################################################

  public void  CentreOnBall(){
    Vector3 cameraPos = golfBall.transform.position + wholeWorldCameraOffset;
    if (golfBall.moveMode!=MoveMode.Frozen){
      cameraPos = TrackingCamera.ZoomInFront2 (
          cameraPos, golfBall.transform.position,
          wholeWorldCameraZoomLayerMask
                                          );
    }   
    transform.position = cameraPos;
  }

} // WholeWorldCamera


} //namespace ChemGolf
