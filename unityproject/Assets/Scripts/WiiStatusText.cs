﻿using UnityEngine;
﻿using UnityEngine.UI;

namespace ChemGolf{

public class WiiStatusText : MonoBehaviour {

  void Update(){
    if (GameManager.instance.wiimote!=null){
      GetComponent<Text>().text = "** Wii connected **";
    } else {
      GetComponent<Text>().text = "No Wii connected";
    }
  }


}


} // namespace ChemGolf
