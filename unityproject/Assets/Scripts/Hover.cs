﻿using UnityEngine;
using System.Collections;
using System;
namespace ChemGolf{
public class Hover : MonoBehaviour {
  public float omega; // In seconds^-1
  public float amplitude; 

  private float initialY_;
  private float initialTime_;

  void Start(){
    initialY_ = transform.position.y;
    initialTime_ = Time.time;
  }

  void Update(){
    Vector3 newPos = transform.position;
    newPos.y = initialY_ + Mathf.Cos ( 
                (Time.time - initialTime_)*omega
                                      )  * amplitude;
    transform.position = newPos;
  }

}
} // namespace ChemGolf
