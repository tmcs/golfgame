﻿﻿namespace ChemGolf{

using UnityEngine;
using System.Collections;

public abstract class Bond: MonoBehaviour {

  public abstract void MoveBond(Vector3 startPos, Vector3 endPos);

  public abstract void SetVisible(bool visible);

}


} //namespace ChemGolf
