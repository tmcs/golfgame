
﻿
namespace ChemGolf{

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BallCoords : MonoBehaviour {

  public GolfBall golfBall;
  public Transform followObject;

  void Update () {

    if (golfBall.modeChooser.viewMode  == ViewMode.WholeMapView) {
      Camera wwc=golfBall.modeChooser.wholeWorldCamera.GetComponent<Camera>();

      Vector2 ViewportPosition=wwc.WorldToViewportPoint(followObject.position);
      GetComponent<Text>().rectTransform.anchorMin = ViewportPosition;  
      GetComponent<Text>().rectTransform.anchorMax = ViewportPosition;   
      GetComponent<Text>().rectTransform.pivot = new Vector2(0f,0.5f); 


    } else {
      GetComponent<Text>().rectTransform.anchorMin = new Vector2(1f,1f);  
      GetComponent<Text>().rectTransform.anchorMax = new Vector2(1f,1f); 
      GetComponent<Text>().rectTransform.pivot = new Vector2(1f,1f); 
    }

    GetComponent<Text>().text = "  Ball position: " +
                 golfBall.GetFractionalPosition ().ToString ("F3");
  }



}
} // namespace ChemGolf



