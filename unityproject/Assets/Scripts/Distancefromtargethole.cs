

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
  using UnityEngine.EventSystems;
﻿namespace ChemGolf{

public class Distancefromtargethole : MonoBehaviour {


	public Image ProgressBar;
	public GameObject goal;
	public GameObject Mainmenupanel;
	public GameObject mainMenuPanelText;
    TrailRenderer Trail;
	public float distancetotarget;

  // ################################################
  // ################################################
  // ################################################
  

	// Use this for initialization
	void Start () {
		SetMenuPanelEnabled (false);
      Trail = gameObject.GetComponent<TrailRenderer>();
	}
	
  // ################################################
  // ################################################
  // ################################################
	// Update is called once per frame
	void Update () {
     

		UpdateDistanceToTarget ();
		CheckDistance ();

    // --------------- show or hide the menu panel -------------------------
    if (Input.GetButtonDown("Cancel")){
      if (GetMenuPanelEnabled()){
        SetMenuPanelEnabled(false);
      } else{
        SetMenuPanelEnabled(true);
          // Deselect and reselect first button to correctly display highlight after enabling parent GO
          EventSystem es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
          es.SetSelectedGameObject(null);
          es.SetSelectedGameObject(es.firstSelectedGameObject);
      }
    }
    // ----------- end show or hide the menu panel -------------------------

	}

  // ################################################
  // ################################################
  // ################################################
	void CheckDistance(){
		ProgressBar.rectTransform.localScale = new Vector3 (distancetotarget / 1000f, ProgressBar.rectTransform.localScale.y, ProgressBar.rectTransform.localScale.z);
		if (distancetotarget < 1f) {
			SetMenuPanelEnabled (true, "Congratulations, you completed the reaction!");
		}
	}
  // ################################################
  // ################################################
  // ################################################
	public void UpdateDistanceToTarget(){
		float distt;
		distt = Vector3.Distance(goal.transform.position,transform.position);
		distancetotarget = distt;
		if (distancetotarget > 1000f) {
			distancetotarget = 1000f;
		}
	}
  // ################################################
  // ################################################
  // ################################################
  // Freeze the scene and bring up a dialog allowing user to 
	public void SetMenuPanelEnabled(bool c, string helptext = "Try to get the ball to the hole."){
		if (c) {
			Time.timeScale = 0.0f;
      GetComponent<GolfBall>().enabled = false;
		} else {
			Time.timeScale = 1.0f;
      GetComponent<GolfBall>().enabled = true;
		}
    mainMenuPanelText.GetComponent<Text>().text = helptext;
		Mainmenupanel.SetActive (c);

	}

  public bool GetMenuPanelEnabled(){
    return Mainmenupanel.activeSelf;
  }

	public void GoToMainMenuPress(){
      GameManager.instance.ChangeLevel("MenuScene");
	}

  public void RespawnPress(){
    SetMenuPanelEnabled(false);
    GetComponent<GolfBall>().Respawn();
      Trail.Clear ();
  }

  public void ResumeGamePress(){
    SetMenuPanelEnabled(false);
  }
  // ################################################
  // ################################################
  // ################################################

  

}

} // namespace ChemGolf
