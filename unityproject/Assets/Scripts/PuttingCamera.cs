﻿using UnityEngine;
using System.Collections;

namespace ChemGolf{

public class PuttingCamera : MonoBehaviour{

  public WiiMoteInputManager wiiManager;
  public float puttingAngle = 0f;
  public float puttingAngleSpeed = 0.05f;

  void FixedUpdate(){
    puttingAngle -= puttingAngleSpeed * ( 
        wiiManager.DpadGetAxis("Horizontal") + Input.GetAxis("Horizontal") 
                                          );
  }

} // PuttingCamera

} // namespace ChemGolf
