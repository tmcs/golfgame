namespace ChemGolf{

  ﻿using UnityEngine;
  using System.Collections;

  public enum TrackingMode {GoThroughHills, ZoomInFrontOfHills2};

  public class TrackingCamera : MonoBehaviour {

    public Transform ball;
    public float distInitial = 10f;
    public float yelev =10f;
    public float inclinationangle =-20f;
    public TrackingMode trackingmode = TrackingMode.ZoomInFrontOfHills2;
    public LayerMask layerMask;

	  void Start (){
	  }


    // Update is called once per frame
	  void Update (){

			  Vector3 off;       // off is vector from camera to ball

			  // --------------- set the camera default position -------------------
			  float theta = GetComponent<PuttingCamera>().puttingAngle;

			  off.x = distInitial * Mathf.Cos (theta);
			  off.z = distInitial * Mathf.Sin (theta);
			  off.y = -yelev;
			  // ----------- end set the camera default position -------------------

			  transform.position = ball.position - off;

			  // --------------- set the camera rotation -------------------------
			  // point straight at the ball
			  Vector3 relativePos = ball.position - transform.position;
			  transform.rotation = Quaternion.LookRotation (relativePos) * Quaternion.Euler (inclinationangle, 0, 0);
			  // ----------- end set the camera rotation -------------------------

			  if (trackingmode == TrackingMode.ZoomInFrontOfHills2) {
          transform.position = ZoomInFront2(transform.position, 
                      ball.position, layerMask ) ;
        }

	  }

  // ################################################
  // ################################################
  // ################################################

    static public  Vector3 ZoomInFront2(Vector3 currentPosition, 
                  Vector3 golfBallpos, LayerMask layerMask){
      // returns new local position of camera.

      float minDistance = 1f; // closest camera can be to ball
      float margin = 1f; // distance in front of hill to zoom to

      // --------------- Zoom in front of any hills ------------------------
      Ray ray = new Ray (golfBallpos,  currentPosition - golfBallpos);
      RaycastHit hit ;
      
      float dist0 = Vector3.Distance(currentPosition , golfBallpos);
      float dist = dist0;
      if (Physics.Raycast (ray, out hit, dist0, layerMask)) {
        if (hit.distance < dist){// we hit something between ball and camera
          if ( hit.distance- margin > minDistance  ){ //, but not too close to ball
            dist = hit.distance -  margin;
          }
        }
      }

      return currentPosition + ((dist0 - dist)/dist0) * (golfBallpos - currentPosition ) ;
      // ----------- end Zoom in front of any hills ------------------------
    }

  // ################################################
  // ################################################
  // ################################################



  }

}//namespace ChemGolf
