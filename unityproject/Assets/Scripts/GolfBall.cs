﻿
namespace ChemGolf{

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Collections;
using System;

public enum MoveMode{Putting, ManualForce, Frozen};

public class GolfBall : MonoBehaviour {

    public WiiMoteInputManager wiiManager;
    public ModeChooser modeChooser;
    public PowerBar powerbar;

    private MoveMode _moveMode;
    public float manualMoveSpeed = 1.0f; // Multiplier for the force
    private Vector3 spawnpoint; // The spawn point
    private Vector3 previousVelocity = new Vector3(0,0,0);
    private float previousTime = 0.0f;

    public float puttdelay = 0.1f;

    public float accelerationFreezeThreshold = 0.1f;
    public bool restartLevelOnStop = false;

    public Temperature temperature;

    public float maxPower = 50f;

    public GameObject wallX0;
    public GameObject wallX1;
    public GameObject wallZ0;
    public GameObject wallZ1;

    private float fast=3f;

    private float tz=0f;

    public float timeLastPutted; // So that we can switch between views with a delay

  // ######################################################################
  // ######################################################################
  // ######################################################################
   public float Xmin(){
     return wallX0.transform.position.x;
   }

   public float Zmin(){
     return wallZ0.transform.position.z;
   }

   public float Xmax(){
     return wallX1.transform.position.x;
   }

   public float Zmax(){
     return wallZ1.transform.position.z;
   }

  // ################################################
  // ################################################
  // ################################################

   void Start () {
     timeLastPutted = -1.0f;
     spawnpoint = GetComponent<Transform>().position;
     Respawn();
   }
   

  // ################################################
  // ################################################
  // ################################################

   void Update () {

    // -------------- get the power from holding the jump button-------------
      if (modeChooser.viewMode == ViewMode.PuttView && Input.GetButtonDown("Jump")){
          powerbar.gameObject.SetActive(true);
          powerbar.power = 0f;
          tz = Time.time;
      } 

      if (modeChooser.viewMode==ViewMode.PuttView && Input.GetButton ("Jump")) {
        powerbar.power =(( Mathf.Cos ((Time.time - tz)*fast+Mathf.PI))+1)*0.5f *maxPower;
      }

      if (modeChooser.viewMode==ViewMode.PuttView && Input.GetButtonUp("Jump")) {
        powerbar.power =(( Mathf.Cos ((Time.time - tz)*fast+Mathf.PI))+1)*0.5f * maxPower;
        PuttBall(powerbar.power*temperature.temperature);
      }
    // -----------end get the power from holding the jump button-------------

    // -------------- get power from wii remote swing ----------------------
    if (modeChooser.viewMode == ViewMode.PuttView && wiiManager.isPressed(WiiMoteInputManager.buttons.b) )
      {
        powerbar.gameObject.SetActive(true);
        powerbar.power = 0f;
      }

      if (modeChooser.viewMode==ViewMode.PuttView && wiiManager.isHeld(WiiMoteInputManager.buttons.b)) {
        powerbar.power = Mathf.Max(powerbar.power,  wiiManager.WiiSwingPower()*maxPower );
      }

    if (modeChooser.viewMode==ViewMode.PuttView && wiiManager.isReleased(WiiMoteInputManager.buttons.b)) {
      wiiManager.toggleReleased(WiiMoteInputManager.buttons.b);
      PuttBall(powerbar.power*temperature.temperature);
    }    
    // -----------end get power from wii remote swing ----------------------
    if (Input.GetButtonDown("moveMode") ){  // XXX this should only be for debugging
      // switch to manual force mode and back
      if (moveMode == MoveMode.ManualForce){
         moveMode = MoveMode.Frozen;
      } else if (moveMode == MoveMode.Putting) {
        moveMode = MoveMode.ManualForce;
      }else if (moveMode == MoveMode.Frozen) {
        moveMode = MoveMode.ManualForce;
      }
    }

    if (Input.GetButtonDown("ballstop") ){
      // Stop the ball
      moveMode = MoveMode.Frozen;
    }


    // -------------- Move the ball with manual forces -------------------
    if (modeChooser.viewMode == ViewMode.WholeMapView) { 
      if (moveMode == MoveMode.ManualForce) {
        Vector3 input;
        Rigidbody ballbody = GetComponent<Rigidbody> ();
        if (ballbody.velocity.magnitude < 
            modeChooser.wholeWorldCamera.GetComponent<WholeWorldCamera>().wholeWorldCameraMaxSpeed) {
          input = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical"));
          ballbody.AddForce (input * manualMoveSpeed);
        }
      }
    }    
    // -----------end Move the ball with manual forces -------------------


   }

// ######################################################################
// ######################################################################
// ######################################################################

  void FixedUpdate(){

    Rigidbody ballbody = GetComponent<Rigidbody> ();
    if (previousTime!=Time.time){
      if (moveMode == MoveMode.Putting){
        if (Time.time - timeLastPutted >0.5f ){
          Vector3 acceleration = (ballbody.velocity - previousVelocity)/(Time.time - previousTime);
          if (acceleration.magnitude < accelerationFreezeThreshold   ){

            // Ball has effectively stopped
            moveMode = MoveMode.Frozen;
            Assert.IsFalse(restartLevelOnStop, "restart level on stop Not yet Implemented!");

          }
        }
      }
    }
    previousVelocity = ballbody.velocity; 
    previousTime = Time.time;
  }

  // ################################################
  // ################################################
  // ################################################

  public MoveMode moveMode{
    get{ 
      return _moveMode;
    } set {
      _moveMode = value;
      if (_moveMode == MoveMode.Frozen){
        GetComponent<Rigidbody> ().isKinematic = true;
        GetComponent<Rigidbody> ().velocity = Vector3.zero;
        modeChooser.viewMode = ViewMode.PuttView;
      } else {
        GetComponent<Rigidbody> ().isKinematic = false;
      } 
    }
  }

  // ################################################
  // ################################################
  // ################################################

 public  void Respawn(){
    transform.position = spawnpoint;
    moveMode = MoveMode.Frozen;
    modeChooser.viewMode = ViewMode.WholeMapView;
  }

  // ################################################
  // ################################################
  // ################################################

    public void PuttBall(float impulsemagnitude){
 
      moveMode = MoveMode.Putting;

      Vector3 impulse;
      float theta = modeChooser.puttingCamera.GetComponent<PuttingCamera>().puttingAngle;
      impulse.x = impulsemagnitude * Mathf.Cos(theta);
      impulse.z = impulsemagnitude * Mathf.Sin(theta);
      impulse.y = 0;

      Rigidbody ballbody = GetComponent<Rigidbody> ();
      ballbody.AddForce (impulse, ForceMode.Impulse);
    
      // switch to world view in switchDelay seconds
      timeLastPutted = Time.time;
    }


  // ################################################
  // ################################################
  // ################################################

/// <summary>
/// Get the fractional position of the golfBall across the Potential Energy
/// surface in each direction.
/// </summary>
    public Vector2 GetFractionalPosition(){
      float xmin = Xmin();
      float zmin = Zmin();
      float xmax = Xmax();
      float zmax = Zmax();

      Vector2 fractionalPosition;
      fractionalPosition.x = (transform.position.x - xmin) / (xmax - xmin)  ; // TODO to optimise, put this as a private variable
      fractionalPosition.y = (transform.position.z - zmin) / (zmax - zmin)  ; // TODO to optimise, put this as a private variable

      if (    fractionalPosition.x < 0.0f || fractionalPosition.x > 1.0f
          ||  fractionalPosition.y < 0.0f || fractionalPosition.y > 1.0f ){
        throw new Exception ("golf ball outside walls!");
      }

      return fractionalPosition;

    }


}


} // namespace ChemGolf
