using UnityEditor;

public class BuildScript
{

// ######################################################################
// ######################################################################
// ######################################################################

  static string[] GetScenes()
  {
    string[] scenes = { "Assets/levels/MenuScene.unity", 
                        "Assets/levels/TestLevel.unity" };
    return scenes;
  }

// ######################################################################
// ######################################################################
// ######################################################################


  static void BuildAllPlatforms(){
     BuildPipeline.BuildPlayer(GetScenes(), "../Build/Linux/golfgame.x86",
                              BuildTarget.StandaloneLinuxUniversal, 
                              BuildOptions.None);
     BuildPipeline.BuildPlayer(GetScenes(), "../Build/Windows32/golfgame.exe",
                              BuildTarget.StandaloneWindows, 
                              BuildOptions.None);
     BuildPipeline.BuildPlayer(GetScenes(), "../Build/Windows64/golfgame.exe",
                              BuildTarget.StandaloneWindows64, 
                              BuildOptions.None);
     BuildPipeline.BuildPlayer(GetScenes(), "../Build/OSX/golfgame",
                              BuildTarget.StandaloneOSXUniversal, 
                              BuildOptions.None);
      EditorApplication.Exit(0);

  }


// ######################################################################
// ######################################################################
// ######################################################################
}

