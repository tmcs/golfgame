			  if (trackingmode == TrackingMode.ZoomInFrontOfHills) {
          off = ZoomInFront ( off, transform.position, golfball.transform.position);
			  }

  // ################################################
  // ################################################
  // ################################################


    static public  Vector3 ZoomInFront(Vector3 off, Vector3 currentPosition, Vector3 golfballpos){
      // off is vector from camera to ball

      // --------------- Zoom in front of any hills ------------------------
      float offset;
      offset = 10f;
      Ray ray = new Ray (golfballpos,  currentPosition - golfballpos);
      RaycastHit hit = new RaycastHit ();
      if (Physics.Raycast (ray, out hit)) {
        if (hit.distance < offset) {
          off.z = (hit.distance - 1f) * Mathf.Sqrt (hit.distance);
          off.y = -(hit.distance - 1f) * Mathf.Sqrt (hit.distance);
        }
      }    
      return off;
      // ----------- end Zoom in front of any hills ------------------------
    }



// ################################################
// ################################################
// ################################################


﻿using UnityEngine;
using System.Collections;

public enum TrackingMode {GoThroughHills, ZoomInFrontOfHills};

public class TrackingCamera : MonoBehaviour {

  public GameObject golfball;
  public GameObject terrain;
  public float distInitial = 10f;
  public float yelev =10f;
  public float inclinationangle;
  public TrackingMode trackingmode;

	void Start (){
	}


  // Update is called once per frame
	void Update (){

			Vector3 pos;

			// --------------- set the camera default position -------------------
			float theta = golfball.GetComponent<GolfBall> ().puttingAngle;
			pos.x = distInitial * Mathf.Cos (theta);
			pos.z = distInitial * Mathf.Sin (theta);
			pos.y = -yelev;
			// ----------- end set the camera default position -------------------

			if (trackingmode == TrackingMode.ZoomInFrontOfHills) {
				// --------------- Zoom in front of any hills ------------------------
				float offset;
				offset = 10f;
				Ray ray = new Ray (golfball.transform.position, transform.position - golfball.transform.position);
				RaycastHit hit = new RaycastHit ();
				if (Physics.Raycast (ray, out hit)) {
					if (hit.distance < offset) {
						pos.z = (hit.distance - 1f) * Mathf.Sqrt (hit.distance);
						pos.y = -(hit.distance - 1f) * Mathf.Sqrt (hit.distance);
					}
				}    
				// ----------- end Zoom in front of any hills ------------------------
			}

			transform.position = golfball.transform.position - pos;

			// --------------- set the camera rotation -------------------------
			// point straight at the ball
			Vector3 relativePos = golfball.transform.position - transform.position;
			transform.rotation = Quaternion.LookRotation (relativePos) * Quaternion.Euler (inclinationangle, 0, 0);
			// ----------- end set the camera rotation -------------------------

	}
}
