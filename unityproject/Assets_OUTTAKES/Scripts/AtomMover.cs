// ######################################################################
// ######################################################################
// ######################################################################
/// <summary>
/// Save all the atom positions to a binary Resource file
/// </summary>
  void SaveGeometry(string fileName){

    // We need to replace this function with a python script that reads a pdb
    // file and outputs a list of atom coordinates
    List<float[]> alist = GetGeometry();
    int nAtom = alist.Count;
    using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create))){
      writer.Write(nAtom);
      for (int i=0; i<nAtom; i++){
        for (int j=0; j<3; j++){
          writer.Write( alist[i][j]);
        }
      }
    }
  }

    if (Input.GetKeyDown("u")){
      // Save the positions to a binary file in a simple binary format
      Debug.Log("Saving positions");
      SaveGeometry("Assets/Resources/timout.bytes");
    }

