  // ################################################
  // ################################################
  // ################################################

  public void PutOnSurface(){
    // XXX DO NOT USE. This function wond handle hills. Need one that moves the ball down til it touches something.
    Vector3 pos = transform.position;
    float margin = 0.1f;
    

		Ray ray = new Ray(pos, Vector3.down);
		RaycastHit hit;
    if (Physics.Raycast (ray, out hit)){
      Vector3 hitpoint = hit.point;
      Ray rayup = new Ray( hitpoint, Vector3.up);
  		RaycastHit hitup;
      if (Physics.Raycast (rayup, out hitup)){
        transform.position = hitpoint + (pos - hitup.point ) + margin*Vector3.up;
      } else {
        throw new Exception ("Ball has no collisions");
      }

    } else {
      throw new Exception ("Ball is not above the surface!");
    }
  }
