  private void createSurface_exponentials() {
    int width = GetComponent<Terrain>().terrainData.heightmapWidth;
    int height = GetComponent<Terrain>().terrainData.heightmapHeight;
    float[,] pesHeights = GetComponent<Terrain>().terrainData.GetHeights (0, 0, width, height);

    float delta = 1.0f / width;

    for (int z_point = 0; z_point < height; z_point++) {
      for (int x_point = 0; x_point < width; x_point++) {
        float x = x_point * delta;
        float y = z_point * delta;

        float exp_1 = 0.5f - 0.5f * Mathf.Exp(
                  -10f 
                  * (x-width*delta*0.75f) 
                  * (x-width*delta*0.75f) - 10.0f * y * y
                                              );
        float exp_2 = 0.5f - 0.5f * Mathf.Exp(
                  -10f * x * x - 10f 
                  * (y-height*delta*0.75f) 
                  * (y-height*delta*0.75f));
        float exp_3 = 1.0f - 1.0f * Mathf.Exp(
              -0.6f * x * x + 1.0f * x * y - 0.65f * y * y
                                              );
        float exp_4 = -0.25f * Mathf.Exp(
              0.7f * x * x + 0.6f * x * y + 0.7f * y * y
                                              );
        float z = exp_1 + exp_2 +exp_4;
       
        pesHeights[x_point,z_point] = z*heightscale;

      }
    }

    /* 

        for (int y = 0; y < height; y++) {
          for (int x = 0; x < width; x++) {
            float sin = Mathf.Sin(y * delta);
            float cos = Mathf.Cos(x * delta);
            pesHeights[x,y] = (0.5f * (sin + cos)/ 250.0f);
        Debug.Log (pesHeights [x, y]);
          }
        }
  */
	
    GetComponent<Terrain>().terrainData.SetHeights( 0, 0, pesHeights);
  }


