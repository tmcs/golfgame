

Atom positions
===============
From AtomSphereStyle.cs:

				o = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				o.transform.position = vl;

Atom colors
==============

from atommodel.cs:

			s_typeToModel.Add("O", new AtomModel("O", 1.52f, 100, new Color(0.827f,0.294f,0.333f,1f), null));
			s_typeToModel.Add("C", new AtomModel("C", 1.70f, 100, new Color(0.282f,0.6f,0.498f,1f)  , null));
			s_typeToModel.Add("N", new AtomModel("N", 1.55f, 100, new Color(0.443f,0.662f,0.882f,1f), null));
			s_typeToModel.Add("H", new AtomModel("H", 1.20f, 100, Color.white                       , null));
			s_typeToModel.Add("S", new AtomModel("S", 2.27f, 100, new Color(1f,0.839f,0.325f,1f)    , null));
			s_typeToModel.Add("P", new AtomModel("P", 1.80f, 100, new Color(0.960f,0.521f,0.313f,1f), null));
			s_typeToModel.Add("X", new AtomModel("X", 1.40f, 100, Color.black, 					null));	
			
from BallUpdateSphere.c

		atomcolor=GetComponent<Renderer>().material.GetColor("_Color");


from SphereManager.c (sp.number is the atomic number):

   sp.GetComponent<Renderer>().material.SetColor("_Color", Molecule.Model.MoleculeModel.atomsColorList[(int)sp.number]);


Bond positions
===================

from BondCubeStyle.cs:

    GameObject o=GameObject.CreatePrimitive(PrimitiveType.Cube);

from HStickManager.cs:

		atomOne = sticks[i].atompointer1.transform.position; // transform.position is costly; this way, we do it twice instead of thrice
		sticks[i].GetComponent<Renderer>().material.SetVector("_TexPos1", atomOne);
		sticks[i].transform.position = atomOne;
		sticks[i].GetComponent<Renderer>().material.SetVector("_TexPos2", sticks[i].atompointer2.transform.position);


from TubeRenderer.cs (called from TubeUpdate.cs which is called from BondTubeStyle.cs):

  rotation = Quaternion.FromToRotation(Vector3.forward, vertices[p+1].point-vertices[p].point);
  meshVertices[vertexIndex] = vertices[p].point + rotation * crossPoints[c] * vertices[p].radius;


    




