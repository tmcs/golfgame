

Bonds cannot move, but they can be enabled and disabled. <<-?
Atoms cannot change type, but they can be enabled and disabled
Bonds cannot change type, but they can be enabled and disabled.

Thus no object creation or destruction happens after the geometry file is loaded.



MoleculeModel (normal class):
+ Atom[] atoms_. Should not change length.
+ Bond[] bonds_. Should not change length.
+ Transform[] bondAtoms_. 
+ Transform parent. // atoms inherit position and size.
------
// Creates a load of GameObjects:
- MoleculeModel( int[] elementNumbers, BondType[] bondTypes, int[] bondAtoms) 
- ~MoleculeModel() // destroys the gameobjects
- SetAtomPositions(float[] positions) // Calls UpdateBondPositions()
- SetBondsVisible(bool[] bondsVisible) 
- SetAtomsVisible(bool[] atomsVisible) 
- atomPositions, getBondsVisible, getAtomsVisible
+ UpdateBondPositions() // This updates all the bonds. For speed, dont call it when
                        // we move each atom individually.

SYMBOLS (may be wrong): + =  private, - = public

/// Updates a molecule model with information from a geometry data file when a
/// golf ball moves.
AtomMover (MonoBehaviour):
+ MoleculeModel model_ // contains number of atoms and number of bonds 
+ TextAsset geometryData_
+ int currentSquare[2]
+ int nX_, nY_
- GolfBall golfball_
----------
- Start() // checks for consistency between the textassets given. Creates a model_.
          // TODO is this called on scene load as well?
+ LoadMoleculeModel(geometryFileName) // calls model_ = AddComponent<MoleculeModel>()
- SetCurrentSquare (int [] square) // calls model_.SetAtomPositions(), model_.SetBondsVisible
                                   //and model_.SetAtomsVisible.


// This class should do as little as possible, for speed.
Atom (MonoBehaviour):
- transform.position
- visible
- material
----------
- Atom(elementNumber, position, visible)
- SetPosition(Vector3 position) // Moves the atom to the Global position. Maybe
                                // remove this method and set position manually.
                                // Just calls transform.localPosition = ...
- SetVisible(const bool visible) 


Bond (MonoBehaviour):
- material
- transform.position
-----------
- Bond(atom1, atom2, BondType)
- SetBondPosition(Transform atom1, Transform atom2) 






