20160401 Tim W
================
Used CMake to automate the build process

20160220  Tim W
=================
- made the menu panel have buttons for restarting the level
- deactivated GolfBall.cs when the menu is open


NOTE: Note that all scripts we have written (ie not UnityMol) should now be surrounded in 
     "namespace GolfGame{...}"

20160217 Tim W and Dom
=========================
Open `GolfGameProjectPleaseWork/EnzymeMolecule.unity`. Here, you can see that pressing
"q" switches between three game modes: WholeMapView, PuttingView, WholeMolecule.

When in WholeMolecule view, you can rotate the camera around the molecule with
the arrow keys. 

Now have a look at the object hierarchy *whilst in play mode* in the editor.

The hierarchy of objects is as shown:

- `MoleculeCentre` an empty game object specifying where the molecule is

  + `LoadBox` a game object which has the molecule loading script attached to it

    * `Camera` the molecule camera. This can be rotated and zoomed when in 
      WholeMolecule mode

      - `Directional light` This is the light that illuminates the molecule

  + `maxCameraTarget` an empty game object specifying the centre of rotation
    for the molecule camera. This should have a local position of (0,0,0)
  
  + `AtomCubeParent` The parent object for all the atoms
    - `Cube` (the first atom)
    - ... etc

- Other objects: CubeBondManager, CubeManager, FieldLineManager, HBallManager,
  ParticleManager, SurfaceManager, etc. 

Your first task is to copy all these objects into `unityproject/enzyme.unity` and 
get it working just like it is in `GolfGameProjectPleaseWork`. After you have done this:

- Make the molecule light so that it doesnt shine on the terrain or ball (using 
  layers and culling masks)

- Apply the HUD to get the molecule superimposed on the golfing view. Pressing "q"
  should still allow you to get the molecule full-screen.

20160212 Tim W and Dom
=======================
Made the whole world camera centred on the ball following the shot. Refactored 
the GolfBall.Update() function into many setter routines.

20160128 - Tim Wiles
=====================
Added keyboard input for putting. See `master` branch

20160128 - Domagoj Fijan
==========================
Added a progress bar. Researched how to include other 3d scenes in the scene

20160128 - Max Howell
==========================
Researched how to get accelerometer data from the wii remote.

20160128 - James Lamb
==========================
Made a menu with level select and quit screen. See `jamesbranch` branch.

20160128 - Jonathan Mannouch
=============================
Researched how to use UnityMol to render the molecule in real-time. See Jonathan's
email.

20160201 - Tim Wiles (extending Jonathan Mannouch's script)
===============================================================
Visualizde the molecule. Enabled the user to move an atom with the "i" and "o" keys.
Added loading ("y") and saving ("u") the atom positions as a proof-of-concept.

Added a python script `jonsrendering/savegeometries.py` that can save atom 
positions in a format readable by Unity.

