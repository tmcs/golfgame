
In this file, put a list of things to test

- The Main Menu. All menu items do the expected thing.
- The in-game menu (press ESC). All menu items do the expected thing. 
- Switching between modes with Q
- Putting the ball (press Q to switch to putt mode then press a number key)
- The on-screen tool-tips eg "Press Q to putt the ball". These change depending
  on the mode.
- The full-screen molecule mode displays a molecule. This can be rotated with the
  arrow keys and zoomed in and out.
- Stopping the ball by pressing Z





