#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function
import subprocess, os, sys
import multiprocessing
import time
import numpy as np
import re


def runentos (infilestr, printstatus=True):
  try:
    entosexe = os.environ["ENTOS"]
  except KeyError:
    print ("Try setting the environment variable ``ENTOS``")
    raise
  cmd = [entosexe]
  p = subprocess.Popen(cmd, 
                  stdin=subprocess.PIPE,
                  stdout=subprocess.PIPE)
  if printstatus: print ( "Running entos on file:\n" + infilestr )
  tstart = time.time()
  outstr, output_err = p.communicate(infilestr)
  timetaken = time.time() - tstart
  assert p.returncode==0, "Entos returned nonzero exit status when processing input file:\n"+infilestr
  if printstatus: print ("Ran entos in {} seconds on file:\n".format(timetaken)+ infilestr )
  return outstr

#########################################################################
#########################################################################
#########################################################################


def getentosenergy(outstr):
  """ Returns None if SCF didnt converge"""
  comp = "TOTAL ENERGY"

  # ---------------- get the energy components ------------------------
  energy = re.findall(comp+r":\s*([\-0-9\.]*)", outstr)
  assert len(energy)==1, "Couldn't find component {} in output file".format(comp)
  if "SCF converged" in outstr:
    return float(energy[0]) 
  else:
    return None
  # ------------ end get the energy components ------------------------

#########################################################################
#########################################################################
#########################################################################

infiletemplate = """
dft(
    structure = '{structurefile}' 
    xc='HF'
    basis = 'aug-cc-pVDZ'
    density_fitting=true
    diis='None'
    ansatz = 'u'
    orbital_grad_threshold=1e-2
    energy_threshold=1e-4
    df_basis = 'aug-cc-pVDZ-JKFIT'
    guess='H0'
  )
"""


def getentosenergies(infiles):

  inputfiles =  [  
        infiletemplate.format(
                          structurefile=structurefile
                                      )
              for structurefile in infiles
              ]
            
  pool = multiprocessing.Pool()
  outfiles = pool.map( runentos , inputfiles )
  np.save("outfiles.npy", outfiles)
  #outfiles = list(np.load("outfiles.npy"))

  energies = np.array([ getentosenergy(outstr) for outstr in outfiles ] , dtype=float)

  return energies

#########################################################################
#########################################################################
#########################################################################



