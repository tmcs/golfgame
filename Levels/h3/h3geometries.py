#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Generate and output the geometries for the h3 reaction


Usage::

    ./h3geometries.py

To re-calculate the energies using entos::

    export ENTOS=../../../../entoswork/entos-newunsoldw12/Debug/entos
    rm h3_energies.npy
    ./h3geometries.py

"""


from __future__ import division, print_function

from savegeometries import saveatompositionsfile, savebondvisibilityfile, saveenergiesfile
import os
import numpy as np
import scipy.interpolate

RESOURCESDIR = "../../unityproject/Assets/Resources"

def savexyzfiles(bondX, bondY):
  xyztemplate = """3

  H 0 0 0
  H {} 0 0
  H {} 0 0"""

  outfilenames = []

  if not os.path.exists("tmp"):
    os.makedirs("tmp")

  for x, bondXval in enumerate(bondX):
    for y, bondYval in enumerate(bondY): 
      outf = os.path.join("tmp","h3_{:03}_{:03}.xyz".format(x, y))
      outfilenames.append(outf)
      open(outf, "w" ).write(xyztemplate.format(bondXval, -bondYval) )

  return outfilenames

#########################################################################
#########################################################################
#########################################################################

def getatompos(bondX, bondY):
  # ---------------- get atom positions array --------------------------
  atompos = np.array( 
                [ 
                  [  ( (0,0,0), 
                       ( bondXval, 0, 0), 
                       ( -bondYval, 0,0) ) 
                    for bondYval in bondY
                  ]
                  for bondXval in bondX
                ]
                      )
  # ------------ end get atom positions array --------------------------
  return atompos
  

 
#########################################################################
#########################################################################
#########################################################################

def getenergies(bondX, bondY):
  from findenergies import getentosenergies
  xyzfiles = savexyzfiles(bondX, bondY)
  return getentosenergies(xyzfiles).reshape(len(bondX), len(bondY))
  
#########################################################################
#########################################################################
#########################################################################

def interpolateenergies(oldX, oldY,newX, newY, energies):
  assert len(oldX.shape)==1
  assert len(oldY.shape)==1
  terrain_energies = scipy.interpolate.RectBivariateSpline(oldX, oldY, energies)
  return terrain_energies( newX[:,None], newY[None,:] )

#########################################################################
#########################################################################
#########################################################################

def main():

  PICOMETRE = 0.01 # in angstroms
  eqbondlength = 74*PICOMETRE
  minbondlength = 0.25*eqbondlength
  maxbondlength = 2.5

  natom = 3
  nbond = 2
  ngeomX = 20
  ngeomY = 20
  nterrainX = 513
  nterrainY = 513

  bondX = np.linspace(minbondlength, maxbondlength, ngeomX)
  bondY = np.linspace(minbondlength, maxbondlength, ngeomY)

  saveatompositionsfile(os.path.join(RESOURCESDIR,"Levels/h3/h3.atompositions.bytes"),
                       getatompos(bondX, bondY) )

  temp = np.zeros( (ngeomX, ngeomY, nbond), dtype=bool)
  temp[:,:,0] = np.tile(np.transpose((bondX<1.6)[np.newaxis]),ngeomY)
  temp[:,:,1] = np.tile((bondY<1.6)[np.newaxis],(ngeomX,1))

  savebondvisibilityfile(os.path.join(RESOURCESDIR,"Levels/h3/h3.bondvisibility.bytes"), temp)

  try:
    energies = np.load("h3_energies.npy")
  except:
    energies = getenergies(bondX, bondY)
    np.save("h3_energies.npy", energies)
 

  # ---------------- put in ceiling and floor ----------------------
  ceiling = -1.59
  floor = -1.618
  energies[np.isnan(energies)] = ceiling
  energies[energies>ceiling] = ceiling
  energies[energies<floor] = floor
  # ------------ end put in ceiling and floor ----------------------

  # ---------------- interpolate energies to make PES ---------------------
  newX = np.linspace(bondX[0], bondX[-1], nterrainX)
  newY = np.linspace(bondY[0], bondY[-1], nterrainY)
  terrain_heights = interpolateenergies(bondX, bondY, newX, newY, energies)
  terrain_heights -= terrain_heights.min() # make minimum zero
  # ------------ end interpolate energies to make PES ---------------------

  saveenergiesfile(os.path.join(RESOURCESDIR,"Levels/h3/h3.energies.bytes"), terrain_heights) 
  #from plotsurface import plotenergies
  #plotenergies(newX, newY, terrain_heights)

#########################################################################
#########################################################################
#########################################################################

if __name__ == '__main__':
  main()




