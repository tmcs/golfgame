#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def plotenergies(bondX, bondY, energies, **kwargs):

  bondX, bondY = np.meshgrid(bondX, bondY)


  fig = plt.figure()
  ax = fig.gca(projection="3d")	

  ax.plot_surface(bondX, bondY, energies, cmap=cm.coolwarm,linewidth=0, antialiased=False, **kwargs)

  plt.show()

