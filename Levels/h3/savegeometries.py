#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Output binary (.bytes) files of molecular geometry data.
    
Note that x and y correspond to x and z (respectively) on the putting green.

"""

from __future__ import division, print_function

import numpy as np


#########################################################################
#########################################################################
#########################################################################


def saveatompositionsfile(fn, atompos):
  ngeomX, ngeomY, natom, ndim = atompos.shape
  assert ndim == 3
  
  with open(fn, "wb") as f:
    np.array([natom], dtype="<i4").tofile(f)
    np.array([ngeomX], dtype="<i4").tofile(f)
    np.array([ngeomY], dtype="<i4").tofile(f)
    atompos.astype( dtype="<f4").tofile(f)

#########################################################################
#########################################################################
#########################################################################

def savebondvisibilityfile(fn, bondvis):
  ngeomX, ngeomY, nbond = bondvis.shape
  with open(fn, "wb") as f:
    np.array([nbond], dtype="<i4").tofile(f)
    np.array([ngeomX], dtype="<i4").tofile(f)
    np.array([ngeomY], dtype="<i4").tofile(f)
    bondvis.astype(dtype="<b").tofile(f)

#########################################################################
#########################################################################
#########################################################################

def saveenergiesfile(fn, energies):
  ngeomX, ngeomY = energies.shape
  with open(fn, "wb") as f:
    np.array([ngeomX], dtype="<i4").tofile(f)
    np.array([ngeomY], dtype="<i4").tofile(f)
    energies.astype( dtype="<f4").tofile(f)


    

      


