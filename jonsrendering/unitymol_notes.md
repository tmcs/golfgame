TODO find out how interactive mode works. Where is the actual MD called from? Put in some debug messages

LateUpdate() of InteractiveStructure() creates an atom on click and creates a bond to it

Molecule3D.cs has a line that calls 			DisplayMolecule.AddAllPhysics(); if UIData.interactive is true

TODO What does Display() of DisplayMolecule.cs do?

Update () of ClickAtom.cs gets info about an atom when it is clicked. If GUIUtility.hotControl, then Destroy(haloList[spot]); 

VRPNAtomSelector.cs also seems to do some forces??

If MouseOverMolecule is added to an object, it makes the object draggable. **We need to add this to an atom.**


createMouseOvers() of HBallmanager.cs creates the interactivity.

ResetPositions() of  HBallmanager.cs resets the positions back to those in atomslocationslist. This is called after interactive mode (or after we have loaded a new trajectory point...)

Alternatively we can callHBallmanager.GetBall(0).transform.position = ...
HBallManager is a GenericManager (Scripts/Molecule/View/GenericManager.cs)

#CreateMouseOversMDDriver() of SphereManager.cs adds #interactivity to the atoms -> But this doesnt seem to output a #log message.... TODO track down where this is done

DisplayMolecule.cs finds the type of UI (aType == UIData.AtomType.hyperball) .
GetManagers() of DisplayMolecule.cs returns a list with 2 items: the atom manager and bond manager.

Trying to get the smooth balls rendering
============================================

SetAtomMenu() in UI/GUIMoleculeController.cs is called by GUIDisplay.cs.

find where toggle_NA_HBALLSMOOTH is set in Loadtypegui.cs.
UI/GUIMoleculeController.cs loads AtomMenu into a GUI window. AtomMenu() calls 
RenderingParameters () and SetAtomStyle(). 

AtomMenu() also sets whether atoms are clickable to 
select them or not.

SetAtomStyle() is also called by some other functions on switch.

However if we wish we can just use toggle_NA_HBALLSMOOTH for smooth hyperballs
UIData.hballsmoothmode = true. UIData is in UI/UIData.cs. The toggle turns smooth
hyperballs on or turns it off and goes back to a default setting in loadtypegui.cs
 line 3251.


The button with the alt text "Change the atom appearance style or rendering method"
brings up the showAtomType box when clicked.

OnGUI() of Molecule3D.cs calls GUIDisplay.Display() if UIData.hiddenUIbutFPS is not true.
Display() of GUIDisplay.cs calls DisplayGui() and SetGuidedMenu(). We need to stop
it doing this.
DisplayGui() of GUIMoleculeController.cs loads MainFun() into a gui window. 
Mainfun() of LoadTypeGui.cs makes the main menu of unitymol. We need to hide this

OnGUI() of Molecule3D.cs also does some other interesting things like clearing
the molecule.

Note that in scenepreload we could have used Molecule3D.loadLoadFile() but this
is not how it is done in the examples.

SetMnipulatormove () of GUIMoleculeController.cs loads the LoadTypeGUI.Manipulator() if  
showManipulatorMenu is false (note the boolean is the opposite of expected!)

iscontrol is set to true by Molecule3D.Display(). I dont think Display() is called.
Update() of Molecule3D.cs  calls KeyOperate and HiddenOperate and MouseOperate 
if isControl is true. 

KeyOperate() of Molecule3D.cs does the moving of the camera when WASD are pressed.
HiddenOperate() of Molecule3D.cs hides the UI when backspace is pressed.

Trying to get the molecule to display spheres
=============================================


Molecule3D.cs calls DisplayMolecule.Display(). This creates a new atomspherestyle 
instance and calls AtomSphereStyle.DisplayAtoms(UIData.atomtype);




Locking the camera
--------------------
public  void GUIMoleculeController.CameraStop () sets maxCamera.cameraStop = true
and UIData.cameraStop2 = true.

Moving all the atoms
------------------------
CreateAtomHB of Atomcubestyle.cs creates an atom. It is then parented to AtomCubeParent.

TODO
=========



