Hi Jonathan,

To get started, run "obabel h2o2.gzmat -Oh2o2.xyz". 
This will convert the z-matrix file to an xyz file.

Then, write a python script that creates lots of z-matrix files  each with 
different values of r1 and r2. Number them h2o2_000_000.gzmat, h2o2_000_001.gzmat... etc.

Then write another script to convert them all to xyz files using openbabel
>>> import subprocess
>>> for i in range(nX):
...   for j in range(nY):
...     subprocess.check_call(["obabel", "h2o2_{:04}_{:04}.gzmat".format(i,j),"-Oh2o2_{:04}_{:04}.xyz".format(i,j) ])


Then write another script to convert the xyz files into a single binary file (see Issue #22).

Best wishes,

Tim
