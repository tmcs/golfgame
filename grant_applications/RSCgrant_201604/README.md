Hannah did a superb job applying for this grant at the end of April 2016 (see
Issue #33).

Sadly we didn't get it :-(

Here is the feedback (almost certainly copy-pasted between all their rejections,
but still helpful):


> Feedback

> To help guide our decision making, two people review each application against 
> specific criteria on our 
> website <http://www.rsc.org/awards-funding/funding/outreach-fund/#eligibility-criteria>. 
> The awarding of funding was particularly competitive in this round and we were 
> able to fund around a third of the small grant applications we received.

> After discussing your application with the review panel, they felt that 
> you had a very innovative project idea, which was an interesting way to 
> communicate about your area of chemistry. However, the reviewers would 
> have liked to have seen more focus in your application on the school and 
> public audiences you intended to engage with during the project, and how 
> you intended to reach them.

> I hope that this feedback is useful to you. There is no limit on how many 
> times an individual or organisation are entitled to apply to the Outreach 
> Fund, so you are welcome to apply, to any of the funding tiers, at any point 
> in the future.

