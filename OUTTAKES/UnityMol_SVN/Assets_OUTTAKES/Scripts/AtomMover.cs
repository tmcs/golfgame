		if (Input.GetKeyDown("g")){
      // Load a pdb file. NOT THE RECOMMENDED WAY
			Debug.Log ("Jon is Loading the Molecule");
			StreamReader sr ;
		    sr=new StreamReader("Assets/retal.pdb");
			ControlMolecule.CreateMolecule(sr);
          DisplayMolecule.GetManagers()[0].ResetPositions();
		}


    if (Input.GetKey("o")){ 
      // Move the first H atom to the right
      Debug.Log("USer pressing o");
      MoleculeModel.atomsLocationlist[0][0] +=0.1f;
      GenericManager manager = DisplayMolecule.GetManagers()[0];
      manager.ResetPositions();
    }

    if (Input.GetKey("i")){
      // Move the first H atom to the left      
      Debug.Log("USer pressing i");
      MoleculeModel.atomsLocationlist[0][0] -=0.1f;
      GenericManager ballmanager = DisplayMolecule.GetManagers()[0];
      ballmanager.ResetPositions();
    }
#

    // --------------- move the camera like in Molecule3D.KeyOperate-----------
    if (Input.GetKey("k")){ // TODO do quaternion rotation thing from Molecule3D.MouseOperate.
      // Also do zooming in and out.
      GameObject LocCamera = GameObject.Find("Camera");
      Vector3 v = LocCamera.transform.localPosition;
		
		  //Molecule right
		  if(Input.GetKey(KeyCode.D))	{
			  v.x-=0.5f;
			  LocCamera.transform.localPosition=v;
		  }
		  //Molecule up
		  if(Input.GetKey(KeyCode.W)) {
			  v.y-=0.5f;
			  LocCamera.transform.localPosition=v;
		  }
		  //Molecule down
		  if(Input.GetKey(KeyCode.S)) {
			  v.y+=0.5f;
			  LocCamera.transform.localPosition=v;
		  }
		  //Molecule left
		  if(Input.GetKey(KeyCode.A)) {
			  v.x+=0.5f;
			  LocCamera.transform.localPosition=v;
		  }
    }
    // ----------- end move the camera like in Molecule3D.KeyOperate-----------


    if (!System.IO.File.Exists(Application.dataPath+"/../sharev/retal.pdb")){
          throw new Exception("Could not find pdb file retal.pdb"); // TODO FileNotFoundException
    }

		//StartCoroutine(requestPDB.LoadPDBWWW("file://"+Application.dataPath+"/../share/retal.pdb"));

		while(!RequestPDB.isDone)
		{
			pdb_progress = requestPDB.progress;
			Debug.Log(pdb_progress);
			yield return new WaitForEndOfFrame();
		}
		pdb_progress = 1.0f;    
