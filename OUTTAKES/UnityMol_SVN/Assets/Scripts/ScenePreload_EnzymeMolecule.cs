using UnityEngine;
using System.Collections;
using UI;
using ParseData.ParsePDB;
using System;

public class ScenePreload_EnzymeMolecule : MonoBehaviour {
	private float pdb_progress = 0;
	private string progresses;

// ######################################################################
// ######################################################################
// ######################################################################

/// <summary>
/// Initialise the TimMolecule scene: load the pdb file and display the molecule.
/// </summary>
  IEnumerator InitScene(RequestPDB requestPDB)
	{

    // --------------- Set user interface and rendering options----------------
		UIData.atomtype = UIData.AtomType.hyperball;
		UIData.bondtype = UIData.BondType.hyperstick;
    UIData.hiddenUI = true;
		GUIMoleculeController.showOpenMenu = false;
		GUIMoleculeController.showAtomMenu = false;
		GUIMoleculeController.globalRadius = 0.3f;
		GUIMoleculeController.shrink = 0.0001f;
		GUIMoleculeController.linkScale = 0.4f;    
    // ----------- end Set user interface and rendering options----------------  

    // --------------- Load the initial PDB from file -----------------------
    string geomdatafn = "retal.pdb";
    TextAsset GeometryData;
    GeometryData = Resources.Load(geomdatafn) as TextAsset; 
    if (GeometryData == null){
      throw new Exception("Could not find resource file "+ geomdatafn);
    }

    yield return new WaitForEndOfFrame();
    requestPDB.LoadPDBResource(geomdatafn);
		pdb_progress = 1.0f;    
    // ----------- end Load the initial PDB from file -----------------------

    // --------------- display the molecule when done ------------------------
		SendMessage("Display",SendMessageOptions.DontRequireReceiver);	    
    // ----------- end display the molecule when done ------------------------

	}
	
// ######################################################################
// ######################################################################
// ######################################################################

	void OnGUI()
	{
		if(pdb_progress >= 1.0f)
			return;
    // Print a loading screen with the pdb load progress.
		progresses = "PDB loading: " + Mathf.CeilToInt(pdb_progress*100) + "%\n";
		progresses = GUI.TextArea(new Rect(Screen.width/2 - 100, Screen.height/2 - 50, 200,100), progresses);
	}
}
