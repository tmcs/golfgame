#!/bin/bash
UNITYEDITOR=/opt/Unity/Editor/Unity
projpath=`pwd`/UnityMol_SVN 
buildname=moleculegame
extraflags=" -logFile buildalllog.log -quit" #-batchmode

platform=Linux64
builddir=build/$platform   
$UNITYEDITOR $extraflags  -projectPath $projpath  -build${platform}Player $builddir/$buildname  && echo "$platform: SUCCESS"

platform=Windows64
builddir=build/$platform   
$UNITYEDITOR $extraflags  -projectPath $projpath  -build${platform}Player $builddir/$buildname && echo "$platform: SUCCESS"


platform=OSX64 
builddir=build/$platform
$UNITYEDITOR $extraflags  -projectPath $projpath  -build${platform}Player $builddir/$buildname

