namespace GolfGame{

﻿using UnityEngine;
using System.Collections;

public enum TrackingMode {GoThroughHills, ZoomInFrontOfHills};

public class TrackingCamera : MonoBehaviour {

  public GameObject golfball;
  public GameObject terrain;
  public float distInitial = 10f;
  public float yelev =10f;
  public float inclinationangle;
  public TrackingMode trackingmode;


  // Update is called once per frame
  void Update (){
		Vector3 pos;

    // --------------- set the camera default position -------------------
    float theta = golfball.GetComponent<GolfBall>().GetPuttingAngle();
		pos.x = distInitial*Mathf.Cos(theta);
		pos.z = distInitial*Mathf.Sin(theta);
		pos.y = -yelev;
    // ----------- end set the camera default position -------------------

    if (trackingmode==TrackingMode.ZoomInFrontOfHills){
      // --------------- Zoom in front of any hills ------------------------
      float offset;
      offset = 10f;
      Ray ray = new Ray (golfball.transform.position, transform.position-golfball.transform.position);
      RaycastHit hit= new RaycastHit();
      if (Physics.Raycast (ray, out hit)) {
        if (hit.distance < offset) {
          pos.z = (hit.distance-1f) * Mathf.Sqrt (hit.distance);
          pos.y = -(hit.distance-1f) * Mathf.Sqrt (hit.distance);
        }
      }    
      // ----------- end Zoom in front of any hills ------------------------
    }

    transform.position = golfball.transform.position - pos;

    // --------------- set the camera rotation -------------------------
    // point straight at the ball
    Vector3 relativePos = golfball.transform.position - transform.position;
    transform.rotation = Quaternion.LookRotation(relativePos)* Quaternion.Euler(inclinationangle, 0, 0)  ;
    // ----------- end set the camera rotation -------------------------

  }
}
}//namespace GolfGame
