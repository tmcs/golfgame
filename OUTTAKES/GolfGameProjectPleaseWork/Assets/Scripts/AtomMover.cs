﻿namespace GolfGame{

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Molecule.Control; // TODO remove when taken out CreateMolecule
using Molecule.Model;
using UnityEngine.Assertions;
using Molecule.View; 
using System.IO;
using System;
using UI;

/// <summary>
/// A class that moves atoms around as the golf ball moves. It gets the geometries
/// from binary files.
/// </summary>
public class AtomMover : MonoBehaviour {

  public GameObject moleculeCentre;
  public GameObject golfBall;
  private TextAsset GeometryData;

  // ################################################
  // ################################################
  // ################################################
  

  void Start(){

    // -------------- load the zone of geometries--------------------------
    string geomdatafn = "timout2";
    GeometryData = Resources.Load(geomdatafn) as TextAsset; // TODO do a loadasync here
    if (GeometryData == null){
      throw new Exception("Could not find resource file "+ geomdatafn);
    }
    // -----------end load the zone of geometries--------------------------

    Molecule.View.DisplayAtom.AtomCubeStyle.AtomCubeParent.transform.parent = moleculeCentre.transform;
    Molecule.View.DisplayAtom.AtomCubeStyle.AtomCubeParent.transform.localPosition = new Vector3(0f,0f,0f);
    Molecule.View.DisplayBond.BondCubeStyle.BondCubeParent.transform.parent = moleculeCentre.transform;
    Molecule.View.DisplayBond.BondCubeStyle.BondCubeParent.transform.localPosition = new Vector3(0f,0f,0f);
  
  }

  // ################################################
  // ################################################
  // ################################################
  

	// Update is called once per frame
	void Update () {

    // TODO do quaternion rotation thing from Molecule3D.MouseOperate
    // --------------- Move the atoms in a few different ways----------------
    if (Input.GetKeyDown("u")){
      // Save the positions to a binary file in a simple binary format
      Debug.Log("Saving positions");
      SaveGeometry("Assets/Resources/timout.bytes");
    }

    if (Input.GetKeyDown("y")){
      // Load the positions from a binary file in golfgeom format
      Debug.Log("Loading positions"); 
      LoadGeometry();
    }

    if (Input.GetKeyDown("l")){
      Molecule.View.DisplayAtom.AtomCubeStyle.AtomCubeParent.transform.localScale*=0.5f;
    }

    GolfBall gb = golfBall.GetComponent<GolfBall>();

    if (Input.GetKey("j") && gb.GetViewMode()==ViewMode.WholeMolecule){
      // Move an atom usign the analog stick
      float moveSpeed = 2f;
		  float moveHorizontal = Input.GetAxis ("Horizontal");
		  float moveVertical = Input.GetAxis ("Vertical");
		  float[] position = MoleculeModel.atomsLocationlist [15];
		  position [0] += moveHorizontal;
		  position [1] += moveVertical;
		  MoleculeModel.atomsLocationlist[15] = position;
      DisplayMolecule.GetManagers()[0].ResetPositions();
      DisplayMolecule.GetManagers()[1].ResetPositions();
    }

    if (!Input.GetKey("j") && gb.GetViewMode()==ViewMode.WholeMolecule){
      // allow the molecule camera to move
      maxCamera.cameraStop = false;
			UIData.cameraStop = false;
			UIData.cameraStop2 = false;      
		} else {
      maxCamera.cameraStop = true;
			UIData.cameraStop = true;
			UIData.cameraStop2 = true;      
    }
    // ----------- end Move the atoms in a few different ways----------------
	

	}
// ######################################################################
// ######################################################################
// ######################################################################
  public void SetGeometry(List<float[]>alist){
    MoleculeModel.atomsLocationlist = alist;
    DisplayMolecule.GetManagers()[0].ResetPositions();    
  }


// ######################################################################
// ######################################################################
// ######################################################################
  public List<float[]> GetGeometry(){
    return MoleculeModel.atomsLocationlist;
  }

// ######################################################################
// ######################################################################
// ######################################################################
/// <summary>
/// Save all the atom positions to a binary Resource file
/// </summary>
  void SaveGeometry(string fileName){

    // We need to replace this function with a python script that reads a pdb
    // file and outputs a list of atom coordinates
    List<float[]> alist = GetGeometry();
    int nAtom = alist.Count;
    using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create))){
      writer.Write(nAtom);
      for (int i=0; i<nAtom; i++){
        for (int j=0; j<3; j++){
          writer.Write( alist[i][j]);
        }
      }
    }
  }

// ######################################################################
// ######################################################################
// ######################################################################
/// <summary>
/// Load all the atom positions form a binary resource file
/// </summary>
  void LoadGeometry(){
    List<float[]> alist = GetGeometry();
    int nAtom = alist.Count;

    Stream s = new MemoryStream(GeometryData.bytes);
    using (BinaryReader reader = new BinaryReader(s)){
      int nAtomStream = reader.ReadInt32(); // The number of atoms.
      Assert.AreEqual(nAtom, nAtomStream);
      for (int i=0; i<nAtom; i++){
        for (int j=0; j<3; j++){
          alist[i][j] = reader.ReadSingle();
        }
      }
    }

    SetGeometry(alist);

  }

// ######################################################################
// ######################################################################
// ######################################################################

}

}
