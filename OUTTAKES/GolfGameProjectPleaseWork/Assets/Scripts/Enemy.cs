﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public Transform[] patrolPoints;

	public Terrain ground;
	private int currentPoint;
	public float moveSpeed;

	// Use this for initialization
	void Start () {
		transform.position = patrolPoints [0].position;
		currentPoint = 0;
	}
	
	// Update is called once per frame
	void Update () {

		Vector2 pos ;
		pos.x = transform.position.x;
		pos.y = transform.position.z;


		Vector2 target ;
		target.x = patrolPoints [currentPoint].position.x;
		target.y = patrolPoints [currentPoint].position.z;

		if (   pos == target ) {
			// Reached the patrol point.
			currentPoint = (currentPoint + 1)%patrolPoints.Length;
		}

		pos = Vector2.MoveTowards ( pos, target, 
					moveSpeed * Time.deltaTime
		);
		PutOnTerrain (pos);
		
	}


	void PutOnTerrain(Vector2 pos){
		Vector3 pos3 ;
		pos3.x = pos .x;
		pos3.y = 3.0f;
		pos3.z = pos .y;


		LayerMask layer = 1 << LayerMask.NameToLayer("terrain");
		Ray ray = new Ray(pos3, Vector3.down);
		//will store info of successful ray cast
		RaycastHit hitInfo;

		if (Physics.Raycast (ray, out hitInfo, layer)) {
			pos3.y = hitInfo.point.y + 1.1f;
		} else {
			print("We missed everything!");
		}
	
		transform.position = pos3;

	}

}
