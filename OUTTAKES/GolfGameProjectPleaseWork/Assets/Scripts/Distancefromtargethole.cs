﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Distancefromtargethole : MonoBehaviour {


	public Image ProgressBar;
	public GameObject golfball;
	public GameObject goal;
	public GameObject Mainmenupanel;

	public float distancetotarget;



	// Use this for initialization
	void Start () {
		ShowMainmenupanel (false);
	}
	
	// Update is called once per frame
	void Update () {
		UpdateDistanceToTarget ();
		CheckDistance ();
          	
	}

	void CheckDistance(){
		ProgressBar.rectTransform.localScale = new Vector3 (distancetotarget / 1000f, ProgressBar.rectTransform.localScale.y, ProgressBar.rectTransform.localScale.z);
		if (distancetotarget < 0.5f) {
			ShowMainmenupanel (true);
		}
	}

	public void UpdateDistanceToTarget(){
		float distt;
		distt = Vector3.Distance(goal.transform.position,golfball.transform.position);
		distancetotarget = distt;
		if (distancetotarget > 1000f) {
			distancetotarget = 1000f;
		}
	}

	public void ShowMainmenupanel(bool c){
		if (c) {
			Time.timeScale = 0.0f;
		} else {
			Time.timeScale = 1.0f;
		}
		Mainmenupanel.SetActive (c);

	}
	public void Restarter(){
		print ("hi all good");
		Application.LoadLevel (Application.loadedLevel);
	}

}
