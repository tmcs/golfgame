﻿
namespace GolfGame{

using UnityEngine;
using System.Collections;



public enum ViewMode{ WholeMapView, PuttView, WholeMolecule };
public enum MoveMode{Putting, ManualForce, Frozen};

public class GolfBall : MonoBehaviour {

  private ViewMode _viewmode;
  private MoveMode _movemode;
  public float moveSpeed = 1.0f; // Multiplier for the force
  private Vector3 spawnpoint; // The spawn point
  private float _puttingAngle = 0f;
  public float puttingAngleSpeed = 1f;

  public float wholeWorldCameraSwitchDelay = 0.8f; // switch to the whole world camera x seconds after putting
  public GameObject wholeWorldCamera; // the camera that shows the whole world
  public GameObject unityMolCamera; // the camera that shows the molecule
  public float wholeWorldCameraSpeedhorizontal = 1f;
  public float wholeWorldCameraSpeedvertical = 1f;
  public float wholeWorldCameraMaxSpeed=10f;


  public float keyImpulseModifier = 1f; // The impulse from a keyboard press
  
  public GameObject puttingCamera;

  private float timeLastPutted; // So that we can switch between views with a delay

  // ################################################
  // ################################################
  // ################################################

   void Start () {
     spawnpoint = GetComponent<Transform>().position;
     timeLastPutted = -1.0f;
   }
   
  // ################################################
  // ################################################
  // ################################################

   public void SetViewMode(ViewMode viewmode){
    _viewmode = viewmode;
      // --------------- Set the active camera -----------------------------
      switch (viewmode) {
        case ViewMode.WholeMapView:
          wholeWorldCamera.GetComponent<Camera>().enabled = true;
          puttingCamera.GetComponent<Camera>().enabled = false;      
          break;  
        case ViewMode.PuttView:
          wholeWorldCamera.GetComponent<Camera>().enabled = false;
          puttingCamera.GetComponent<Camera>().enabled = true;
          break;
        case ViewMode.WholeMolecule:
          wholeWorldCamera.GetComponent<Camera>().enabled = false;
          puttingCamera.GetComponent<Camera>().enabled = false;
          break;
      }
      // ----------- end Set the active camera -----------------------------
   }


  public ViewMode GetViewMode(){
    return _viewmode;
  }

  // ################################################
  // ################################################
  // ################################################

   public void SetMoveMode(MoveMode movemode){
      _movemode = movemode;
      if (movemode == MoveMode.Frozen){
        GetComponent<Rigidbody> ().isKinematic = true;
        GetComponent<Rigidbody> ().velocity = Vector3.zero;
      } else {
        GetComponent<Rigidbody> ().isKinematic = false;
      } 
    }

  public MoveMode GetMoveMode(){
    return _movemode;
  }

  // ################################################
  // ################################################
  // ################################################


  public float GetPuttingAngle(){
    return _puttingAngle;
  }
 
  public void SetPuttingAngle(float theta){
    _puttingAngle = theta;
  }
 

  // ################################################
  // ################################################
  // ################################################

   void Update () {


    if (Input.GetKeyDown("c")){
      Respawn();
    }

    if (Input.GetKeyDown("q") ){ 
      // switch to whole world view and back
      switch (GetViewMode()) {
        case ViewMode.WholeMapView:
          SetViewMode(ViewMode.PuttView);
          break;
        case ViewMode.PuttView:
          SetViewMode(ViewMode.WholeMolecule);
          break;
        case ViewMode.WholeMolecule:
          SetViewMode(ViewMode.WholeMapView);
          break;
      }
    }

    if (Input.GetKeyDown("e") ){  // XXX this should only be for debugging
      // switch to manual force mode and back
      if (GetMoveMode() == MoveMode.ManualForce){
         SetMoveMode(MoveMode.Frozen);
      } else if (GetMoveMode() == MoveMode.Putting) {
        SetMoveMode(MoveMode.ManualForce);
      }else if (GetMoveMode() == MoveMode.Frozen) {
        SetMoveMode(MoveMode.ManualForce);
      }
    }  

    if (Input.GetKeyDown("z") ){
      // Stop the ball
      SetMoveMode(MoveMode.Frozen);
    }

    // --------------- behaviour of trigger -------------------------------
    if (Input.GetButtonDown("Fire1")) {
      if ( GetViewMode()==ViewMode.WholeMapView) { 
        SetViewMode(ViewMode.PuttView);
      }else {
        // We are swinging the club. TODO do something
      }
    }
    // ----------- end behaviour of trigger -------------------------------


    // --------------- behaviour of arrow keys -----------------------------
    if ( GetViewMode()==ViewMode.WholeMapView) { 
      if (GetMoveMode()==MoveMode.ManualForce ){
        // Move the ball with manual forces from the arrow keys
        Vector3 input;
        Rigidbody ballbody = GetComponent<Rigidbody> ();
          if (ballbody.velocity.magnitude < wholeWorldCameraMaxSpeed) {
             input = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical"));
             ballbody.AddForce (input * moveSpeed);
          }
      } else {
        // move the whole world camera
        Vector3 pos = wholeWorldCamera.transform.position;
        pos.x += Input.GetAxis("Horizontal") * wholeWorldCameraSpeedhorizontal;
        pos.z += Input.GetAxis ("Vertical") * wholeWorldCameraSpeedvertical;
        wholeWorldCamera.transform.position = pos;
      }
    } else {
      // Move the putting arrow left and right
      SetPuttingAngle(GetPuttingAngle() - puttingAngleSpeed * Input.GetAxis ("Horizontal"));
    }
    // ----------- end behaviour of arrow keys -----------------------------

    // --------------- behaviour of keyboard numbers --------------------
    for (int ii =1; ii<=9; ii++){  
      if (Input.GetKeyDown(""+ii)){
        if ( GetViewMode()==ViewMode.PuttView) { 
          SetMoveMode(MoveMode.Putting);
          // Fire the ball with the impulse
          PuttBall(ii * keyImpulseModifier  );
        }
      }
    }
    // ----------- end behaviour of keyboard numbers --------------------


    // --------------- switch back to world view after putting----------------
    if (   timeLastPutted > 0.0f ){
      if ( timeLastPutted + wholeWorldCameraSwitchDelay <  Time.time  ) {
        SetViewMode(ViewMode.WholeMapView);
        timeLastPutted = -1.0f;
        // TODO centre camera on ball after shot. 
      }
    }
    // ----------- end switch back to world view after putting----------------

   }

  // ################################################
  // ################################################
  // ################################################

 public  void Respawn(){
    transform.position = spawnpoint;
    SetMoveMode(MoveMode.Frozen);
  }

  // ################################################
  // ################################################
  // ################################################


  void WholeWorldCameraCentreOnBall(){
    // TODO    

  }

  // ################################################
  // ################################################
  // ################################################

   void OnCollisionEnter(Collision Other){
      if (Other.transform.tag == "hole") {
         print ("You succeeded at the game!");
      // Go to the next level
      }
   }

  // ################################################
  // ################################################
  // ################################################

  public void PuttBall(float impulsemagnitude){
    Debug.Log ("Ball putted with impulse "+ impulsemagnitude);

    Vector3 impulse;
    float theta = GetPuttingAngle();
    impulse.x = impulsemagnitude * Mathf.Cos(theta);
    impulse.z = impulsemagnitude * Mathf.Sin(theta);
    impulse.y = 0;

    Rigidbody ballbody = GetComponent<Rigidbody> ();
    ballbody.AddForce (impulse, ForceMode.Impulse);
  
    // switch to world view in switchDelay seconds
    timeLastPutted = Time.time;
  }

  // ################################################
  // ################################################
  // ################################################


}


} // namespace GolfGame
